package org.darkstorm.runescape.script.agility;

import java.awt.Point;
import java.util.*;

import org.darkstorm.runescape.api.input.*;
import org.darkstorm.runescape.api.util.*;
import org.darkstorm.runescape.api.wrapper.*;

public class GnomeAgilityCourse extends AgilityCourse {
	private static final Tile LOG_LOCATION = new Tile(2474, 3435);
	private static final Tile TIGHTROPE_LOCATION = new Tile(2478.5, 3420.4, 2);
	private static final Tile SECOND_NET_LOCATION = new Tile(2474, 3428);
	private static final TileArea PIPE_AREA = new TileArea(
			new Tile(2489, 3432), new Tile(2482, 3430));
	private static final TileArea INNER_PIPE_AREA = new TileArea(new Tile(2488,
			3435), new Tile(2483, 3432));
	private static final TileArea FIRST_NET_AREA = new TileArea(new Tile(2489,
			3427), new Tile(2482, 2425));
	private static final TileArea SECOND_NET_AREA = new TileArea(new Tile(2477,
			3426), new Tile(2470, 3424));
	private static final TileArea LOG_START_AREA = new TileArea(new Tile(2476,
			3439), new Tile(2472, 3436));
	private static final TileArea COURSE_AREA = new TileArea(new Tile(2469,
			3414), new Tile(2490, 3440));
	private static final int FIRST_PIPE_ID = 4058;
	private static final int SECOND_PIPE_ID = 154;
	private static final int[] NET_IDS = { 2285, 2286 };
	private static final int FIRST_TREE_BRANCH_ID = 2313;
	private static final int SECOND_TREE_BRANCH_ID = 2314;

	private final Filter<GameObject> firstTreeBranchFilter,
			secondTreeBranchFilter, firstNetFilter, secondNetFilter,
			firstPipeFilter, secondPipeFilter;

	private boolean lastObstacle = false;

	public GnomeAgilityCourse(DarkAgility script) {
		super(script);
		firstTreeBranchFilter = filters.chain(
				filters.object(FIRST_TREE_BRANCH_ID),
				filters.<GameObject> area(COURSE_AREA));
		secondTreeBranchFilter = filters.chain(
				filters.object(SECOND_TREE_BRANCH_ID),
				filters.<GameObject> area(COURSE_AREA));
		firstNetFilter = filters.chain(filters.object(NET_IDS),
				filters.<GameObject> area(FIRST_NET_AREA));
		secondNetFilter = filters.chain(filters.object(NET_IDS),
				filters.<GameObject> area(SECOND_NET_AREA));
		firstPipeFilter = filters.object(FIRST_PIPE_ID);
		secondPipeFilter = filters.object(SECOND_PIPE_ID);
	}

	@Override
	public boolean activate() {
		Player self = players.getSelf();
		return !self.isMoving() && self.getAnimation() == -1
				&& !INNER_PIPE_AREA.contains(self.getLocation())
				&& getTarget() != null;
	}

	@Override
	public void run() {
		CourseObstacle target = getTarget();
		if(target == null)
			return;
		MouseTarget mouseTarget = target.getTarget();
		if(mouseTarget == null)
			return;
		Point screenLocation = mouseTarget.getLocation();
		if(screenLocation == null
				|| !calculations.isInGameArea(screenLocation)
				|| (players.getSelf().getLocation()
						.distanceTo(target.getLocation()) > 4 && calculations
						.random(1, 4) == 2)) {
			if(players.getSelf().getLocation().distanceTo(target.getLocation()) > 8) {
				walking.walkTo(target.getLocation().randomize(1.5));
				calculations.sleep(1000, 1500);
				return;
			} else {
				camera.turnTo(target);
				camera.setAngleYTo(target);
			}
			calculations.sleep(250, 500);
		}
		mouseTarget = target.getTarget();
		if(mouseTarget == null) {
			walking.walkTo(target.getLocation().randomize(1.5));
			calculations.sleep(1000, 1500);
			return;
		}
		screenLocation = mouseTarget.getLocation();
		if(screenLocation == null || !calculations.isInGameArea(screenLocation)) {
			walking.walkTo(target.getLocation().randomize(1.5));
			calculations.sleep(500, 1200);
			return;
		} else if(target.locatable instanceof GameObject
				&& secondNetFilter.accept((GameObject) target.locatable)
				&& players.getSelf().getLocation().getY() <= 3425) {
			walking.walkTo(SECOND_NET_LOCATION.randomize(1.5));
			calculations.sleep(500, 1200);
			return;
		} else if(calculations.random(1, 20) == 5) {
			camera.turnTo(target);
			camera.setAngleYTo(target);
		}
		if(!menu.perform(target, target.getAction()))
			return;
		if(target.locatable instanceof GameObject
				&& (firstPipeFilter.accept((GameObject) target.locatable) || secondPipeFilter
						.accept((GameObject) target.locatable))) {
			lastObstacle = true;
		} else if(target.locatable instanceof TileAreaLocatable
				&& ((TileAreaLocatable) target.locatable).area == LOG_START_AREA
				&& lastObstacle) {
			script.incrementRounds();
			lastObstacle = false;
		}
		calculations.sleep(1500, 3000);
		Player self = players.getSelf();
		while(self.isMoving() || self.getAnimation() != -1)
			calculations.sleep(10, 25);
	}

	private CourseObstacle getTarget() {
		GameObject firstNet = gameObjects.getClosest(firstNetFilter);
		GameObject secondNet = gameObjects.getClosest(secondNetFilter);
		GameObject firstTreeBranch = gameObjects
				.getClosest(firstTreeBranchFilter);
		GameObject secondTreeBranch = gameObjects
				.getClosest(secondTreeBranchFilter);
		GameObject pipe = gameObjects
				.getClosest(calculations.random(0, 2) == 1 ? firstPipeFilter
						: secondPipeFilter);
		List<CourseObstacle> obstacles = new ArrayList<>();
		if(firstNet != null)
			obstacles.add(new CourseObstacle("Climb-over Obstacle net",
					firstNet));
		if(secondNet != null)
			obstacles.add(new CourseObstacle("Climb-over Obstacle net",
					secondNet));
		if(firstTreeBranch != null)
			obstacles.add(new CourseObstacle("Climb Tree branch",
					firstTreeBranch));
		if(secondTreeBranch != null)
			obstacles.add(new CourseObstacle("Climb-down Tree branch",
					secondTreeBranch));
		if(pipe != null)
			obstacles.add(new CourseObstacle("Squeeze-through Obstacle pipe",
					pipe));
		TileAreaLocatable logArea = new TileAreaLocatable(LOG_START_AREA);
		obstacles.add(new CourseObstacle("Walk-across Log balance", logArea,
				new TileMouseTarget(context, LOG_LOCATION, 0.4, 0.4, -0.3)));
		obstacles.add(new CourseObstacle("Walk-on Balancing rope",
				TIGHTROPE_LOCATION, new TileMouseTarget(context,
						TIGHTROPE_LOCATION, 0.1, 0.1, -0.1)));
		CourseObstacle closest = getClosest(obstacles);
		if(closest == null)
			return null;
		Player self = players.getSelf();
		Tile location = self.getLocation();
		if(closest.locatable == firstNet
				&& firstNet.getLocation().getPreciseX() > 2480
				&& location.getPreciseY() > firstNet.getLocation()
						.getPreciseY()) {
			for(CourseObstacle obstacle : obstacles)
				if(obstacle.locatable == pipe)
					closest = obstacle;
		} else if(closest.locatable == pipe
				&& !PIPE_AREA.contains(pipe.getLocation())) {
			for(CourseObstacle obstacle : obstacles)
				if(obstacle.locatable == logArea)
					closest = obstacle;
		} else if(closest.locatable == logArea
				&& location.getPreciseY() < LOG_LOCATION.getPreciseY()) {
			for(CourseObstacle obstacle : obstacles)
				if(obstacle.locatable == secondNet)
					closest = obstacle;
		}
		return closest;
	}

	private <T extends Locatable> T getClosest(List<T> targets) {
		Tile origin = players.getSelf().getLocation();
		T closest = null;
		double closestDistance = 0;
		for(T target : targets) {
			Tile location = target.getLocation();
			if(origin.getPlane() != location.getPlane())
				continue;
			double distance = origin.distanceTo(location);
			if(closest == null || distance < closestDistance) {
				closest = target;
				closestDistance = distance;
			}
		}
		return closest;
	}

	@Override
	public TileArea getArea() {
		return COURSE_AREA;
	}

	private class CourseObstacle implements Locatable, MouseTargetable {
		private final String action;
		private final Locatable locatable;
		private final MouseTargetable targetable;

		public <T extends Locatable & MouseTargetable> CourseObstacle(
				String action, T target) {
			this.action = action;
			locatable = target;
			targetable = target;
		}

		public CourseObstacle(String action, Locatable locatable,
				MouseTargetable targetable) {
			this.action = action;
			this.locatable = locatable;
			this.targetable = targetable;
		}

		public String getAction() {
			return action;
		}

		@Override
		public Tile getLocation() {
			return locatable.getLocation();
		}

		@Override
		public MouseTarget getTarget() {
			return targetable.getTarget();
		}
	}

	private class TileAreaLocatable implements Locatable {
		private final TileArea area;

		public TileAreaLocatable(TileArea area) {
			this.area = area;
		}

		@Override
		public Tile getLocation() {
			return area.getRandomTileInside();
		}
	}
}
