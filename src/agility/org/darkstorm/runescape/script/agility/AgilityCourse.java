package org.darkstorm.runescape.script.agility;

import java.util.logging.Logger;

import org.darkstorm.runescape.Bot;
import org.darkstorm.runescape.api.*;
import org.darkstorm.runescape.api.input.*;
import org.darkstorm.runescape.api.util.TileArea;
import org.darkstorm.runescape.script.Task;

public abstract class AgilityCourse implements Task {
	protected final DarkAgility script;
	protected final Bot bot;
	protected final GameContext context;

	protected final Calculations calculations;
	protected final Game game;
	protected final Players players;
	protected final NPCs npcs;
	protected final GameObjects gameObjects;
	protected final GroundItems groundItems;
	protected final Skills skills;
	protected final Interfaces interfaces;
	protected final Menu menu;
	protected final Camera camera;
	protected final Inventory inventory;
	protected final Bank bank;
	protected final Walking walking;
	protected final Mouse mouse;
	protected final Keyboard keyboard;
	protected final Settings settings;
	protected final Filters filters;

	protected final Logger logger;

	public AgilityCourse(DarkAgility script) {
		this.script = script;
		bot = script.getBot();

		context = script.getContext();
		calculations = context.getCalculations();
		game = context.getGame();
		players = context.getPlayers();
		npcs = context.getNPCs();
		gameObjects = context.getGameObjects();
		groundItems = context.getGroundItems();
		skills = context.getSkills();
		interfaces = context.getInterfaces();
		menu = context.getMenu();
		camera = context.getCamera();
		inventory = context.getInventory();
		bank = context.getBank();
		walking = context.getWalking();
		mouse = context.getMouse();
		keyboard = context.getKeyboard();
		settings = context.getSettings();
		filters = context.getFilters();

		logger = script.getLogger();
	}

	public abstract TileArea getArea();
}
