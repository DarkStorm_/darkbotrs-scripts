package org.darkstorm.runescape.script.agility;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;

import org.darkstorm.runescape.api.input.RectangleMouseTarget;
import org.darkstorm.runescape.api.tab.*;
import org.darkstorm.runescape.api.util.Skill;
import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;
import org.darkstorm.runescape.script.paint.*;
import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.component.Frame;
import org.darkstorm.runescape.script.paint.component.Label;
import org.darkstorm.runescape.script.paint.component.Panel;
import org.darkstorm.runescape.script.paint.component.basic.*;
import org.darkstorm.runescape.script.paint.theme.simple.SimpleTheme;

@ScriptManifest(
		name = "DarkAgility",
		authors = "DarkStorm",
		description = "Build up your skill in the most repetitive activity in the game!",
		category = ScriptCategory.AGILITY, version = "1.12")
public class DarkAgility extends AbstractScript {
	private AgilityCourse course;

	private Paint paint;
	private Frame frame;
	private Label timeElapsedLabel, experienceLabel, levelLabel, roundsLabel,
			roundsPerHourLabel, levelPerHourLabel, experiencePerHourLabel;
	private Panel statusPanel;

	private long startTime;
	private int rounds = 0;
	private int startExperience;
	private int startLevel;

	public DarkAgility(ScriptManager manager) {
		super(manager);

		course = new GnomeAgilityCourse(this);
	}

	@Override
	protected void onStart() {
		rounds = 0;
		startTime = System.currentTimeMillis();
		logger.info("Started!");

		startExperience = skills.getExperience(Skill.AGILITY);
		startLevel = skills.getActualLevel(Skill.AGILITY);

		paint = new BasicPaint(this);
		paint.setTheme(new SimpleTheme());
		frame = new BasicFrame("DarkAgility " + getManifest().version());
		paint.addComponent(frame);
		Font font = new Font("Arial", Font.PLAIN, 14);
		try {
			font = Font
					.createFont(
							Font.TRUETYPE_FONT,
							getClass().getResource("/neuropol x free.ttf")
									.openStream()).deriveFont(14f);
		} catch(FontFormatException e1) {
			e1.printStackTrace();
		} catch(IOException e1) {
			e1.printStackTrace();
		}
		frame.setFont(font.deriveFont(Font.BOLD));
		frame.add(timeElapsedLabel = new BasicLabel("Time running: 0:00:00"));
		frame.add(new BasicLabel(" "));
		frame.add(roundsLabel = new BasicLabel("Rounds: 0"));
		frame.add(roundsPerHourLabel = new BasicLabel("  Per hour: ?"));
		frame.add(levelLabel = new BasicLabel("Level gain: 0"));
		frame.add(levelPerHourLabel = new BasicLabel("  Per hour: ?"));
		frame.add(experienceLabel = new BasicLabel("Experience gain: 0"));
		frame.add(experiencePerHourLabel = new BasicLabel("  Per hour: ?"));
		frame.add(new BasicLabel(" "));
		BasicLabel status = new BasicLabel("Status:");
		frame.add(status);
		status.setFont(status.getFont().deriveFont(Font.BOLD));
		frame.add(statusPanel = new BasicPanel());
		frame.setX(547);
		frame.setY(205);
		frame.setWidth(190);
		frame.setHeight(261);
		frame.setClosable(false);
		frame.setPinnable(false);
		frame.layoutChildren();
		frame.setVisible(true);

		registerTasks();
	}

	private void registerTasks() {
		for(Task task : getRegisteredTasks())
			deregister(task);
		register(new BranchTask(new WalkTask(), course, new BanPreventionTask()));
	}

	@Override
	protected void onStop() {
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics2D graphics = (Graphics2D) event.getGraphics();
		if(frame == null || !frame.isVisible())
			return;
		graphics.setStroke(new BasicStroke(1.0f));

		long time = (System.currentTimeMillis() - startTime) / 1000;
		long hours = time / 60 / 60, minutes = (time / 60) - (hours * 60), seconds = time
				- (minutes * 60) - (hours * 60 * 60);
		StringBuffer text = new StringBuffer();
		text.append(hours).append(":").append(minutes < 10 ? "0" : "")
				.append(minutes).append(":").append(seconds < 10 ? "0" : "")
				.append(seconds);
		timeElapsedLabel.setText("Time running: " + text.toString());

		int levels = 0, experience = 0;
		levels += skills.getActualLevel(Skill.AGILITY) - startLevel;
		experience += skills.getExperience(Skill.AGILITY) - startExperience;
		roundsLabel.setText("Rounds: " + rounds);
		roundsPerHourLabel.setText("  Per hour: " + perHour(rounds));
		levelLabel.setText("Level gain: " + levels);
		levelPerHourLabel.setText("  Per hour: " + perHour(levels));
		experienceLabel.setText("Experience gain: " + experience);
		experiencePerHourLabel.setText("  Per hour: " + perHour(experience));

		for(Component child : statusPanel.getChildren())
			statusPanel.remove(child);
		List<Task> tasks = new ArrayList<>();
		for(Task task : getActiveTasks())
			if(task instanceof BranchTask)
				getActiveTasks((BranchTask) task, tasks);
			else
				tasks.add(task);
		for(Task task : tasks) {
			if(task instanceof AgilityCourse)
				statusPanel.add(new BasicLabel("Training agility..."));
			else if(task instanceof WalkTask)
				statusPanel.add(new BasicLabel("Walking to agility course..."));
		}
		statusPanel.resize();
		frame.layoutChildren();

		paint.render(graphics);
	}

	private int perHour(int value) {
		return (int) (60 * 60 * 1000 * (value / (double) (System
				.currentTimeMillis() - startTime)));
	}

	private void getActiveTasks(BranchTask task, List<Task> activeTasks) {
		Task subtask = task.getActiveTask();
		if(subtask == null)
			return;
		if(subtask instanceof BranchTask)
			getActiveTasks((BranchTask) subtask, activeTasks);
		else
			activeTasks.add(subtask);
	}

	public synchronized void incrementRounds() {
		rounds++;
	}

	private class WalkTask implements Task {
		@Override
		public boolean activate() {
			AgilityCourse course = DarkAgility.this.course;
			return course != null
					&& !course.getArea().contains(
							players.getSelf().getLocation());
		}

		@Override
		public void run() {
			AgilityCourse course = DarkAgility.this.course;
			if(course == null)
				return;
			walking.walkTo(course.getArea().getRandomTileInside());
			sleep(1000, 1500);
		}
	}

	private class BanPreventionTask implements Task {
		private long lastAntiBan, nextAntiBan;

		public BanPreventionTask() {
			lastAntiBan = System.currentTimeMillis() - startTime;
			nextAntiBan = random(60000, 120000);
		}

		@Override
		public boolean activate() {
			return (System.currentTimeMillis() - startTime) - lastAntiBan >= nextAntiBan;
		}

		@Override
		public void run() {
			try {
				int randomMode = random(0, 6);
				if(!game.isLoggedIn())
					randomMode = 1;
				if(randomMode == 0 || randomMode == 1 || randomMode == 2) {
					int random = random(0, 8);
					char[] keys = new char[random > 3 ? 2 : 1];
					keys[0] = (char) (KeyEvent.VK_LEFT + (random % 4));
					if(random > 3)
						keys[1] = (char) (KeyEvent.VK_LEFT + ((random % 2) * 2));
					for(int i = 0; i < keys.length; i++)
						keyboard.pressKey(keys[i]);
					sleep(100, 1500);
					for(int i = keys.length - 1; i >= 0; i--)
						keyboard.releaseKey(keys[i]);
				} else if(randomMode == 3 || randomMode == 4) {
					for(int i = 0; i < random(1, 7); i++) {
						mouse.moveRandomly(400);
						sleep(0, 250);
					}
				} else if(randomMode == 5) {
					int random = random(1, 10);
					Tab tab;
					if(random <= 5)
						tab = game.getTab(SkillTab.class);
					else if(random == 6 || random == 7)
						tab = game.getTab(FriendTab.class);
					else if(random == 8)
						tab = game.getTab(CombatTab.class);
					else
						tab = game.getTab(EquipmentTab.class);
					tab.open();
					if(random(1, 5) != 2) {
						mouse.move(new RectangleMouseTarget(tab.getTabArea()));
						mouse.await();
					}
					sleep(3000, 6000);
					if(random(1, 5) != 2) {
						game.getTab(InventoryTab.class).open();
						sleep(250, 500);
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			lastAntiBan = System.currentTimeMillis() - startTime;
			nextAntiBan = random(500, 40000);
		}
	}
}
