import java.awt.*;

import org.darkstorm.runescape.api.wrapper.GameObject;
import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;

@ScriptManifest(name = "GameObject Debug", authors = "DarkStorm",
		description = "asdf", version = "1.0")
public class GameObjectDebugScript extends AbstractScript {
	public GameObjectDebugScript(ScriptManager manager) {
		super(manager);
	}

	@Override
	protected void onStart() {
		logger.info("Started!");
	}

	@Override
	protected void onStop() {
		logger.info("Stopped!");
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics g = event.getGraphics();
		((Graphics2D) g).setStroke(new BasicStroke(1.0f));
		FontMetrics metrics = g.getFontMetrics();
		for(GameObject object : gameObjects.getAll()) {
			if(object == null)
				return;
			Point location = object.getScreenLocation();
			g.setColor(Color.GREEN);
			String str = object.getId() + " (" + object.getType().toString()
					+ ")";
			g.drawString(str, location.x - metrics.stringWidth(str) / 2,
					location.y - metrics.getHeight() * 3 / 2);
		}
	}
}
