/*	
	Copyright 2012 Jan Ove Saltvedt
	
	This file is part of KBot.

    KBot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KBot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KBot.  If not, see <http://www.gnu.org/licenses/>.
	
*/

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.*;

import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.script.*;

/**
 * Created by IntelliJ IDEA. User: Jan Ove Saltvedt Date: Oct 20, 2009 Time:
 * 5:45:27 PM To change this template use File | Settings | File Templates.
 */
@ScriptManifest(name = "Interface Debugger", authors = "DarkStorm",
		description = "Find interfaces", version = "1.0")
public class InterfaceDebugger extends AbstractScript implements
		TreeSelectionListener {
	@SuppressWarnings("unused")
	private boolean shallRun;
	private InterfaceExplorer UI;
	private InterfaceComponent currentSelectedIComponent;

	public InterfaceDebugger(ScriptManager manager) {
		super(manager);
	}

	/**
	 * Is called right before the run() gets called
	 */
	@Override
	public void onStart() {
		shallRun = true;
		UI = new InterfaceExplorer();

		UI.interfaceTree.addTreeSelectionListener(this);
		UI.resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateTree(new IComponentFilter() {
					@Override
					public boolean includeICompontent(
							InterfaceComponent iComponent, InterfaceExplorer ui) {
						return true;
					}
				});
			}
		});
		UI.filterByTextMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final String search = JOptionPane.showInputDialog(UI,
						"Enter the text you want to seach for.",
						"Filter by text", JOptionPane.PLAIN_MESSAGE)
						.toLowerCase();
				if(search == null) {
					JOptionPane.showMessageDialog(UI,
							"You got to write something into the box!");
					return;
				}
				updateTree(new IComponentFilter() {
					@Override
					public boolean includeICompontent(
							InterfaceComponent iComponent, InterfaceExplorer ui) {
						String text = iComponent.getText();
						if(iComponent.getActions() != null
								&& iComponent.getActions().length > 0) {
							for(String s : iComponent.getActions()) {
								text = text + s;
							}
						}
						text = text + iComponent.getSelectedAction();
						return text != null
								&& text.toLowerCase().contains(search);

					}
				});
			}
		});
		UI.setVisible(true);
		updateTree(new IComponentFilter() {
			@Override
			public boolean includeICompontent(InterfaceComponent iComponent,
					InterfaceExplorer ui) {
				return true;
			}
		});
		UI.reflectionExlporeMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/*DefaultMutableTreeNode node = (DefaultMutableTreeNode) UI.interfaceTree
						.getLastSelectedPathComponent();
				if(node == null) {
					return;
				}
				Object nodeObject = node.getUserObject();
				if(nodeObject instanceof IComponentNode) {
					IComponentNode iComponentNode = (IComponentNode) nodeObject;
					RS2InterfaceChild iComponent = iComponentNode.getIComponent();
					Object o = iComponent.getClientObject();
					ReflectionExplorer reflectionExplorer = new ReflectionExplorer(
							botEnv);
					WrappedObject wrappedObject = new WrappedObject(o, botEnv,
							null);
					DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode(
							wrappedObject);
					reflectionExplorer.tree.setModel(new DefaultTreeModel(
							defaultMutableTreeNode));
					reflectionExplorer.setVisible(true);
				}*/
			}
		});
	}

	private void updateTree(IComponentFilter filter) {
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Root");
		Interface[] ifaces = interfaces.getInterfaces();
		java.util.List<Interface> loadedInterfaces = new LinkedList<Interface>();
		for(Interface anInterface : ifaces) {
			if(anInterface == null) {
				continue;
			}
			boolean shallAdd = false;
			for(InterfaceComponent child : anInterface.getComponents()) {
				if(filter.includeICompontent(child, UI)) {
					shallAdd = true;
				}
			}
			if(shallAdd) {
				loadedInterfaces.add(anInterface);
			}
		}
		if(loadedInterfaces.isEmpty()) {
			return;
		}
		fillTree(rootNode, loadedInterfaces);
		UI.interfaceTree.setModel(new DefaultTreeModel(rootNode));
		UI.interfaceTree.updateUI();
	}

	private void fillTree(DefaultMutableTreeNode rootNode,
			java.util.List<Interface> loadedInterfaces) {
		for(final Interface anInterface : loadedInterfaces) {
			DefaultMutableTreeNode groupNode = new DefaultMutableTreeNode(
					new Object() {
						@Override
						public String toString() {
							return "Interface[" + anInterface.getId() + "]";
						}
					});
			fillInterfaceComponents(groupNode, anInterface);
			rootNode.add(groupNode);
		}
	}

	private void fillInterfaceComponents(DefaultMutableTreeNode groupNode,
			Interface anInterface) {
		InterfaceComponent[] components = anInterface.getComponents();
		for(int i = 0; i < components.length; i++) {
			final InterfaceComponent iComponent = components[i];
			if(iComponent == null) {
				continue;
			}
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(
					new IComponentNode() {
						@Override
						public String toString() {
							return "[" + iComponent.getId() + "]";
						}

						@Override
						public InterfaceComponent getIComponent() {
							return iComponent;
						}
					});
			// if(iComponent.hasChildren()) {
			// fillChildren(node, iComponent);
			// }
			groupNode.add(node);
		}

	}

	/*	private void fillChildren(DefaultMutableTreeNode parrentNode,
				RS2InterfaceChild parrent) {
			RS2InterfaceChild[] components = parrent.getChildren();
			for(int i = 0; i < components.length; i++) {
				final RS2InterfaceChild iComponent = components[i];
				if(iComponent == null) {
					continue;
				}
				final int index = i;
				DefaultMutableTreeNode node = new DefaultMutableTreeNode(
						new IComponentNode() {
							@Override
							public String toString() {
								return "[" + index + "]";
							}

							@Override
							public RS2InterfaceChild getIComponent() {
								return iComponent; // To change body of implemented
													// methods use File | Settings |
													// File Templates.
							}
						});
				if(iComponent.hasChildren()) {
					fillChildren(node, iComponent);
				}
				parrentNode.add(node);
			}
		}*/

	/**
	 * Is called to stop the debugger. The debugger is than added to the cleanup
	 * queue and thread will be force killed if not deleted within 10 seconds.
	 */
	@Override
	protected void onStop() {
		shallRun = false;
		UI.setVisible(false);
	}

	/**
	 * Gets called when the client updates it graphics. Please do not do
	 * anything extremely time consuming in here as it will make the fps go low.
	 * 
	 * @param g
	 *            Graphics to paint on
	 */
	@EventHandler
	public void onPaint(org.darkstorm.runescape.event.game.PaintEvent event) {
		Graphics g = event.getGraphics();
		if(currentSelectedIComponent != null) {
			Rectangle rectangle = currentSelectedIComponent.getBounds();
			if(rectangle != null) {
				g.setColor(Color.orange);
				g.drawRect(rectangle.x, rectangle.y, rectangle.width,
						rectangle.height);
			}
		}
	}

	/**
	 * Called whenever the value of the selection changes.
	 * 
	 * @param e
	 *            the event that characterizes the change.
	 */
	@SuppressWarnings("serial")
	@Override
	public void valueChanged(TreeSelectionEvent e) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) UI.interfaceTree
				.getLastSelectedPathComponent();
		if(node == null) {
			return;
		}
		Object nodeObject = node.getUserObject();
		if(nodeObject instanceof IComponentNode) {
			InterfaceComponent face = ((IComponentNode) nodeObject)
					.getIComponent();
			currentSelectedIComponent = face;
			List<Object[]> data = new LinkedList<Object[]>();
			data.add(new Object[] { "".equals("") ? "The interface is valid."
					: "The interface is not valid!" });
			data.add(new Object[] { face.isValid() ? "The interface is visible."
					: "The interface is not visible!" });
			Rectangle rect = face.getBounds();
			if(rect != null) {
				data.add(new Object[] { "Screen position:" });
				data.add(new Object[] { "    x: " + rect.getX() });
				data.add(new Object[] { "    y: " + rect.getY() });
				data.add(new Object[] { "    width: " + rect.getWidth() });
				data.add(new Object[] { "    height: " + rect.getHeight() });
				data.add(new Object[] { "" });
			}
			data.add(new Object[] { "Relative position:" });
			data.add(new Object[] { "    x: " + face.getRelativeBounds().x });
			data.add(new Object[] { "    y: " + face.getRelativeBounds().y });
			data.add(new Object[] { "" });
			data.add(new Object[] { "Text:" });
			data.add(new Object[] { "    Message: " + face.getText() });
			data.add(new Object[] { "    Text color: " + face.getTextColor() });
			data.add(new Object[] { "" });
			data.add(new Object[] { "Other:" });
			data.add(new Object[] { "    Type: " + face.getType() });
			data.add(new Object[] { "    Tooltip: " + face.getTooltip() });
			data.add(new Object[] { "    Selected action: "
					+ face.getSelectedAction() });
			data.add(new Object[] { "    Model ID: " + face.getModelId() });
			data.add(new Object[] { "    Model type: " + face.getModelType() });
			// data.add(new Object[] { "    Hidden until mouse over: "
			// + face.getHiddenUntilMouseOver() });
			// data.add(new Object[] { "    Mouse over interface: "
			// + face.getMouseOverPopupInterface() });
			data.add(new Object[] { "" });
			String[] actions = face.getActions();
			if(actions != null) {
				data.add(new Object[] { "Actions:" });
				for(String action : actions) {
					data.add(new Object[] { "        " + action });
				}
			}
			UI.infoTable.setModel(new DefaultTableModel(data
					.toArray(new Object[1][1]),
					new String[] { "Interface information" }) {
				@SuppressWarnings("rawtypes")
				final Class[] columnTypes = new Class[] { String.class };
				final boolean[] columnEditable = new boolean[] { false };

				@Override
				public Class<?> getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}

				@Override
				public boolean isCellEditable(int rowIndex, int columnIndex) {
					return columnEditable[columnIndex];
				}
			});

		} else {
			UI.infoTable.setModel(new DefaultTableModel(
					new Object[][] { { "Select an interface." }, },
					new String[] { "Interface information" }) {
				@SuppressWarnings("rawtypes")
				final Class[] columnTypes = new Class[] { String.class };
				final boolean[] columnEditable = new boolean[] { false };

				@Override
				public Class<?> getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}

				@Override
				public boolean isCellEditable(int rowIndex, int columnIndex) {
					return columnEditable[columnIndex];
				}
			});
		}
	}

	private abstract class IComponentNode {
		public abstract InterfaceComponent getIComponent();
	}

}

interface IComponentFilter {
	public boolean includeICompontent(InterfaceComponent iComponent,
			InterfaceExplorer ui);
}
