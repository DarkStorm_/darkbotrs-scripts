import java.awt.*;

import org.darkstorm.runescape.api.util.Tile;
import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.event.*;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;

@ScriptManifest(name = "Test!", authors = "DarkStorm", description = "asdf", version = "1.0")
public class TestScript extends AbstractScript {
	Tile bankPos = new Tile(3185, 3436);

	public TestScript(ScriptManager manager) {
		super(manager);
	}

	@Override
	protected void onStart() {
		logger.info("Started!");
		register(new TestTask());
	}

	@Override
	protected void onStop() {
		logger.info("Stopped!");
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics g = event.getGraphics();
		Point mpos = mouse.getLocation();
		((Graphics2D) g).setStroke(new BasicStroke(1.0f));
		g.setColor(Color.WHITE);
		g.drawLine(mpos.x, 0, mpos.x, bot.getGame().getHeight());
		g.drawLine(0, mpos.y, bot.getGame().getWidth(), mpos.y);

		for(NPC npc : npcs.getAll(filters.<NPC> range(10))) {
			if(npc == null)
				return;
			Model model = npc.getModel();
			Color cyan = Color.RED;
			g.setColor(new Color(cyan.getRed(), cyan.getGreen(), cyan.getBlue()));
			Polygon hull = model.getHull();
			if(hull != null)
				g.drawPolygon(hull);
			Point center = model.getCenterPoint();
			if(center != null)
				g.drawOval(center.x - 3, center.y - 3, 6, 6);
		}
	}

	private class TestTask implements Task {
		private final Tile lumbridge = new Tile(3222, 3219),
				falador = new Tile(2964, 3381);

		@Override
		public boolean activate() {
			return true;
		}

		@Override
		public void run() {
			System.out.println("Walking...");
			try {
				if(players.getSelf().getLocation().distanceTo(lumbridge) > 10)
					walking.walkTo(lumbridge);
				else
					walking.walkTo(falador);
			} catch(Exception e) {
				e.printStackTrace();
			}
			sleep(5000, 10000);
		}
	}
}
