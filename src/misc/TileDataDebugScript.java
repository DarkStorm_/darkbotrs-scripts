import java.awt.*;

import org.darkstorm.runescape.api.util.Tile;
import org.darkstorm.runescape.api.wrapper.Player;
import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;

@ScriptManifest(name = "Tile Data Debug", authors = "DarkStorm",
		description = "asdf", version = "1.0")
public class TileDataDebugScript extends AbstractScript {
	public TileDataDebugScript(ScriptManager manager) {
		super(manager);
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics g = event.getGraphics();
		((Graphics2D) g).setStroke(new BasicStroke(1.0f));
		g.setClip(calculations.getGameArea());
		g.setFont(g.getFont().deriveFont(Font.PLAIN));
		int[][] tileData = game.getTileCollisionData();
		Player self = players.getSelf();
		Tile location = self.getLocation();
		for(int x = location.getX() - 15; x < location.getX() + 15; x++) {
			for(int y = location.getY() - 15; y < location.getY() + 15; y++) {
				Point screen = calculations.getLimitlessWorldScreenLocation(
						x + 0.5, y + 0.5, 0);
				String text = Integer.toHexString(tileData[x
						- game.getRegionBaseX()][y - game.getRegionBaseY()]);
				FontMetrics metrics = g.getFontMetrics();
				g.drawString(text, screen.x - metrics.stringWidth(text) / 2,
						screen.y - metrics.getHeight() * 3 / 2);

				Point[] points = new Point[] {
						calculations.getLimitlessWorldScreenLocation(x, y, 0),
						calculations.getLimitlessWorldScreenLocation(x + 1, y,
								0),
						calculations.getLimitlessWorldScreenLocation(x + 1,
								y + 1, 0), };
				Point last = points[0];
				for(int i = 1; i < points.length; i++) {
					Point point = points[i];
					if((last.x == -1 && last.y == -1)
							|| (point.x == -1 && point.y == -1))
						break;
					g.drawLine(last.x, last.y, point.x, point.y);
					last = point;
				}
			}
		}
	}
}
