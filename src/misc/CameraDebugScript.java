import java.awt.*;

import org.darkstorm.runescape.api.util.Tile;
import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;

@ScriptManifest(name = "Camera Debug", authors = "DarkStorm",
		description = "asdf", version = "1.0")
public class CameraDebugScript extends AbstractScript {
	public CameraDebugScript(ScriptManager manager) {
		super(manager);
	}

	@Override
	protected void onStart() {
		logger.info("Started!");
	}

	@Override
	protected void onStop() {
		logger.info("Stopped!");
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics g = event.getGraphics();
		((Graphics2D) g).setStroke(new BasicStroke(1.0f));
		g.drawString("Camera X: " + camera.getAngleX(), 5, 20);
		g.drawString("Camera Y: " + camera.getAngleY(), 5, 35);
		for(GameObject object : gameObjects.getAll(filters
				.<GameObject> range(7))) {
			render(g, object);
		}
	}

	private void render(Graphics g, GameObject object) {
		Player self = players.getSelf();
		Tile location = self.getLocation();
		Tile objectLocation = object.getLocation();
		double height = calculations.getTileHeight(location) / 128D + 1.0;
		double objectHeight = calculations.getTileHeight(objectLocation) / 128D;
		double angle = Math.toDegrees(-Math.atan2(height + objectHeight,
				location.distanceTo(objectLocation)));
		Point screen = calculations.getLimitlessWorldScreenLocation(
				location.getPreciseX(), location.getPreciseY(), 128);
		Point floorScreen = calculations.getLimitlessWorldScreenLocation(
				location.getPreciseX(), location.getPreciseY(), 0);
		Point objectScreen = calculations.getLimitlessWorldScreenLocation(
				objectLocation.getPreciseX(), objectLocation.getPreciseY(), 0);
		// if(!calculations.isInGameArea(screen)
		// || !calculations.isInGameArea(floorScreen)
		// || !calculations.isInGameArea(objectScreen))
		// return;
		g.drawLine(objectScreen.x, objectScreen.y, screen.x, screen.y);
		g.drawLine(objectScreen.x, objectScreen.y, floorScreen.x, floorScreen.y);
		String text = Double.toString(angle);
		FontMetrics metrics = g.getFontMetrics();
		g.drawString(text, objectScreen.x - metrics.stringWidth(text) / 2,
				objectScreen.y - metrics.getHeight() * 3 / 2);
	}
}
