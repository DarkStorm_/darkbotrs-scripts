import java.awt.*;

import org.darkstorm.runescape.api.wrapper.Item;
import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;

@ScriptManifest(name = "Inventory Debug", authors = "DarkStorm",
		description = "asdf", version = "1.0")
public class InventoryDebugScript extends AbstractScript {
	public InventoryDebugScript(ScriptManager manager) {
		super(manager);
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics g = event.getGraphics();
		((Graphics2D) g).setStroke(new BasicStroke(1.0f));
		g.setFont(g.getFont().deriveFont(Font.PLAIN));
		FontMetrics metrics = g.getFontMetrics();
		Item[] items = inventory.getItems();
		for(Item item : items) {
			if(item == null)
				continue;
			g.setColor(Color.GREEN);
			item.getTarget().render(g);
			Point location = item.getScreenLocation();
			String str = Integer.toString(item.getId());
			if(item.getStackSize() == 1) {
				location.translate(-metrics.stringWidth(str) / 2,
						metrics.getHeight() / 2);
				g.setColor(Color.BLACK);
				g.drawString(str, location.x + 1, location.y + 1);
				g.setColor(Color.YELLOW);
				g.drawString(str, location.x, location.y);
			} else {
				String str2 = " x" + item.getStackSize();
				location.translate(-metrics.stringWidth(str2) / 2,
						(2 * metrics.getHeight() + 4) / 2);
				g.setColor(Color.BLACK);
				g.drawString(str2, location.x + 1, location.y + 1);
				g.setColor(Color.YELLOW);
				g.drawString(str2, location.x, location.y);
				location.translate(
						metrics.stringWidth(str2) / 2
								- metrics.stringWidth(str) / 2,
						-metrics.getHeight() - 4);
				g.setColor(Color.BLACK);
				g.drawString(str, location.x + 1, location.y + 1);
				g.setColor(Color.YELLOW);
				g.drawString(str, location.x, location.y);
			}
		}
	}
}
