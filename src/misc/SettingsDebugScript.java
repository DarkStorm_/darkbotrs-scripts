import java.awt.*;
import java.util.Arrays;

import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;

@ScriptManifest(name = "Settings", authors = "DarkStorm", description = "Debug settings", version = "1.0")
public class SettingsDebugScript extends AbstractScript {
	private static final Font monoFont = Font.decode(Font.MONOSPACED);
	private static final int TIMEOUT = 5000;

	private int[] lastSettings = new int[0];
	private long[] settingsAge = new long[0];

	public SettingsDebugScript(ScriptManager manager) {
		super(manager);
	}

	@Override
	protected void onStart() {
		logger.info("Started!");
		setRefreshDelay(250);
	}

	@Override
	protected void onStop() {
		logger.info("Stopped!");
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics g = event.getGraphics();
		int[] settings = new int[this.settings.getCount()];
		final Font prev = g.getFont();
		g.setFont(monoFont);
		if(settings.length > lastSettings.length) {
			lastSettings = Arrays.copyOf(lastSettings, settings.length);
			settingsAge = Arrays.copyOf(settingsAge, settings.length);
		}
		int id = 0;
		final long curTime = System.currentTimeMillis();
		final long cutoffTime = curTime - TIMEOUT;
		for(int i = 0; i < settings.length; i++) {
			settings[i] = this.settings.get(i);
			if(settingsAge[i] == 0) {
				settingsAge[i] = settings[i] != 0 ? cutoffTime : curTime;
			}
			if(lastSettings[i] != settings[i]) {
				settingsAge[i] = curTime;
				lastSettings[i] = settings[i];
			}
			final boolean highlight = settingsAge[i] > cutoffTime;
			final boolean show = settings[i] != 0;
			if(show || highlight) {
				final int x = 10 + 140 * (id % 4);
				final int y = 70 + 12 * (id / 4);
				final String s = String.format("%4d: %d", i, settings[i]);
				g.setColor(Color.black);
				g.drawString(s, x, y + 1);
				g.setColor(highlight ? Color.red : Color.green);
				g.drawString(s, x, y);
				id++;
			}
		}
		g.setFont(prev);
	}
}
