import java.awt.*;

import org.darkstorm.runescape.api.util.Tile;
import org.darkstorm.runescape.event.*;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;

@ScriptManifest(name = "Walking Test!", authors = "DarkStorm", description = "asdf", version = "1.0")
public class WalkingTestScript extends AbstractScript {
	Tile bankPos = new Tile(3185, 3436);

	public WalkingTestScript(ScriptManager manager) {
		super(manager);
	}

	@Override
	protected void onStart() {
		register(new TestTask());
	}

	@Override
	protected void onStop() {
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics g = event.getGraphics();
		Point mpos = mouse.getLocation();
		g.setColor(Color.WHITE);
		g.drawLine(mpos.x, 0, mpos.x, bot.getGame().getHeight());
		g.drawLine(0, mpos.y, bot.getGame().getWidth(), mpos.y);
	}

	private class TestTask implements Task {
		@Override
		public boolean activate() {
			return players.getSelf().getLocation().distanceTo(bankPos) > 10;
		}

		@Override
		public void run() {
			System.out.println("Moving...");
			walking.walkTo(bankPos);
		}
	}
}
