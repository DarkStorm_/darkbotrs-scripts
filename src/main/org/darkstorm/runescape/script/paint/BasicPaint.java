package org.darkstorm.runescape.script.paint;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.event.MouseInputListener;

import org.darkstorm.runescape.script.Script;
import org.darkstorm.runescape.script.paint.component.*;
import org.darkstorm.runescape.script.paint.theme.Theme;

public class BasicPaint implements Paint, MouseInputListener {
	private final java.awt.Component source, listener;
	private final List<Component> components;
	private final Graphics tempGraphics;

	private java.awt.Point mouseLocation = new java.awt.Point(0, 0);
	private boolean[] pressed = new boolean[3];
	private Theme theme;

	private boolean disposed = false;

	public BasicPaint(java.awt.Component source) {
		this(source, source);
	}

	public BasicPaint(Script script) {
		this(script.getBot().getGame(), script.getBot().getCanvas());
	}

	public BasicPaint(java.awt.Component source, java.awt.Component listener) {
		this.source = source;
		this.listener = listener;
		components = new CopyOnWriteArrayList<Component>();
		tempGraphics = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
				.createGraphics();

		listener.addMouseListener(this);
		listener.addMouseMotionListener(this);
	}

	@Override
	public void addComponent(Component component) {
		component.setPaint(this);
		component.setTheme(theme);
		components.add(0, component);
	}

	@Override
	public void removeComponent(Component component) {
		components.remove(component);
	}

	@Override
	public Component[] getComponents() {
		return components.toArray(new Component[components.size()]);
	}

	@Override
	public void bringForward(Component component) {
		if(components.remove(component))
			components.add(0, component);
	}

	@Override
	public Theme getTheme() {
		return theme;
	}

	@Override
	public void setTheme(Theme theme) {
		this.theme = theme;
		for(Component component : components)
			component.setTheme(theme);
	}

	@Override
	public void render(java.awt.Graphics2D graphics) {
		if(disposed)
			return;
		graphics.setRenderingHint(
				java.awt.RenderingHints.KEY_TEXT_ANTIALIASING,
				java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		Component[] components = getComponents();
		for(int i = components.length - 1; i >= 0; i--)
			components[i].render(graphics);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(disposed)
			return;
		pressed[e.getButton() - 1] = true;
		int x = e.getPoint().x;
		int y = e.getPoint().y;
		mouseLocation = e.getPoint();
		for(Component component : getComponents()) {
			if(!component.isVisible() || !(component instanceof Container))
				continue;
			Container container = (Container) component;
			if((!(component instanceof Frame) || ((Frame) component)
					.isMinimized()) && !component.getArea().contains(x, y)) {
				for(Component child : container.getChildren()) {
					for(java.awt.Rectangle area : child.getTheme()
							.getUIForComponent(child)
							.getInteractableRegions(child)) {
						if(area.contains(x - component.getX() - child.getX(), y
								- component.getY() - child.getY())) {
							component.onMousePress(x - component.getX(), y
									- component.getY(), e.getButton() - 1);
							bringForward(component);
							return;
						}
					}
				}
			}
		}
		for(Component component : getComponents()) {
			if(!component.isVisible())
				continue;
			if(!(component instanceof Frame && ((Frame) component)
					.isMinimized()) && component.getArea().contains(x, y)) {
				component.onMousePress(x - component.getX(),
						y - component.getY(), e.getButton() - 1);
				bringForward(component);
				break;
			} else if(component instanceof Frame
					&& ((Frame) component).isMinimized()) {
				for(java.awt.Rectangle area : component.getTheme()
						.getUIForComponent(component)
						.getInteractableRegions(component)) {
					if(area.contains(x - component.getX(), y - component.getY())) {
						component.onMousePress(x - component.getX(), y
								- component.getY(), e.getButton() - 1);
						bringForward(component);
						return;
					}
				}
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(disposed)
			return;
		pressed[e.getButton() - 1] = false;
		int x = e.getPoint().x;
		int y = e.getPoint().y;
		mouseLocation = e.getPoint();
		for(Component component : getComponents()) {
			if(!component.isVisible() || !(component instanceof Container))
				continue;
			Container container = (Container) component;
			if((!(component instanceof Frame) || ((Frame) component)
					.isMinimized()) && !component.getArea().contains(x, y)) {
				for(Component child : container.getChildren()) {
					for(java.awt.Rectangle area : child.getTheme()
							.getUIForComponent(child)
							.getInteractableRegions(child)) {
						if(area.contains(x - component.getX() - child.getX(), y
								- component.getY() - child.getY())) {
							component.onMouseRelease(x - component.getX(), y
									- component.getY(), e.getButton() - 1);
							bringForward(component);
							return;
						}
					}
				}
			}
		}
		for(Component component : getComponents()) {
			if(!component.isVisible())
				continue;
			if(!(component instanceof Frame && ((Frame) component)
					.isMinimized()) && component.getArea().contains(x, y)) {
				component.onMouseRelease(x - component.getX(),
						y - component.getY(), e.getButton() - 1);
				bringForward(component);
				break;
			} else if(component instanceof Frame
					&& ((Frame) component).isMinimized()) {
				for(java.awt.Rectangle area : component.getTheme()
						.getUIForComponent(component)
						.getInteractableRegions(component)) {
					if(area.contains(x - component.getX(), y - component.getY())) {
						component.onMouseRelease(x - component.getX(), y
								- component.getY(), e.getButton() - 1);
						bringForward(component);
						return;
					}
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(disposed)
			return;
		mouseLocation = e.getPoint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if(disposed)
			return;
		mouseLocation = e.getPoint();
	}

	@Override
	public java.awt.Point getMouseLocation() {
		return new java.awt.Point(mouseLocation);
	}

	@Override
	public boolean isButtonPressed(int button) {
		if(button < 1 || button > 3)
			return false;
		return pressed[button - 1];
	}

	@Override
	public java.awt.Component getSource() {
		return source;
	}

	@Override
	public Graphics getGraphics() {
		Graphics graphics = source.getGraphics();
		return graphics != null ? graphics : tempGraphics;
	}

	@Override
	public void dispose() {
		disposed = true;
		listener.removeMouseListener(this);
		listener.removeMouseMotionListener(this);
	}
}
