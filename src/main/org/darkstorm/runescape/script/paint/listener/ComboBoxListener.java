package org.darkstorm.runescape.script.paint.listener;

import org.darkstorm.runescape.script.paint.component.ComboBox;

public interface ComboBoxListener extends ComponentListener {
	public void onComboBoxSelectionChanged(ComboBox comboBox);
}
