package org.darkstorm.runescape.script.paint.listener;

import org.darkstorm.runescape.script.paint.component.SelectableComponent;

public interface SelectableComponentListener extends ComponentListener {
	public void onSelectedStateChanged(SelectableComponent component);
}
