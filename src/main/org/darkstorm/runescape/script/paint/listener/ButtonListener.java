package org.darkstorm.runescape.script.paint.listener;

import org.darkstorm.runescape.script.paint.component.Button;

public interface ButtonListener extends ComponentListener {
	public void onButtonPress(Button button);
}
