package org.darkstorm.runescape.script.paint;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.swing.*;
import javax.swing.event.MouseInputListener;

import org.darkstorm.runescape.script.paint.component.*;
import org.darkstorm.runescape.script.paint.component.Button;
import org.darkstorm.runescape.script.paint.component.basic.*;
import org.darkstorm.runescape.script.paint.listener.*;
import org.darkstorm.runescape.script.paint.theme.Theme;
import org.darkstorm.runescape.script.paint.theme.simple.SimpleTheme;

@SuppressWarnings({ "serial", "unused" })
public class PaintTest extends java.awt.Canvas implements MouseInputListener,
		KeyListener {
	private int x = 0, y = 0;
	private BufferedImage image;
	private Graphics2D g;

	private boolean firstTime = true;
	private boolean pressed = false;

	private Paint paint;

	public PaintTest() {
		JFrame canvas = new JFrame("Canvas");
		setSize(765, 528);
		setIgnoreRepaint(true);
		canvas.add(this);
		canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		canvas.pack();

		paint = new BasicPaint(this);
		paint.setTheme(new SimpleTheme());
		createTestFrame();
		BasicButton button = new BasicButton("Test button");
		button.setPaint(paint);
		button.setTheme(paint.getTheme());
		button.setX(100);
		button.setY(100);
		button.addButtonListener(new ButtonListener() {

			@Override
			public void onButtonPress(Button button) {
				System.out.println("PRESS!");
			}
		});
		paint.addComponent(button);

		canvas.setVisible(true);
		requestFocus();

		Timer timer = new Timer(20, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		});
		timer.setRepeats(true);
		timer.setCoalesce(true);
		timer.start();
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
	}

	private void createTestFrame() {
		org.darkstorm.runescape.script.paint.component.Frame testFrame = new BasicFrame(
				"Frame");
		testFrame.setPaint(paint);
		testFrame.setTheme(paint.getTheme());
		testFrame.add(new BasicLabel("TEST LOL"));
		testFrame.add(new BasicLabel("TEST 23423"));
		testFrame.add(new BasicLabel("TE123123123ST LOL"));
		testFrame.add(new BasicLabel("31243 LO3242L432"));
		BasicButton testButton = new BasicButton("Duplicate this frame!");
		testButton.addButtonListener(new ButtonListener() {

			@Override
			public void onButtonPress(
					org.darkstorm.runescape.script.paint.component.Button button) {
				createTestFrame();
			}
		});
		testFrame.add(new BasicCheckButton("This is a checkbox"));
		testFrame.add(testButton);
		ComboBox comboBox = new BasicComboBox("Simple theme", "Other theme",
				"Other theme 2");
		comboBox.addComboBoxListener(new ComboBoxListener() {

			@Override
			public void onComboBoxSelectionChanged(ComboBox comboBox) {
				Theme theme;
				switch(comboBox.getSelectedIndex()) {
				case 0:
					theme = new SimpleTheme();
					break;
				case 1:
					// Some other theme
					// break;
				case 2:
					// Another theme
					// break;
				default:
					return;
				}
				paint.setTheme(theme);
			}
		});
		testFrame.add(comboBox);
		testFrame.setX(50);
		testFrame.setY(50);
		testFrame.resize();
		testFrame.layoutChildren();
		testFrame.setVisible(true);
		// testFrame.setMinimized(true);
		paint.addComponent(testFrame);
	}

	public static void main(String[] args) {
		new PaintTest();

	}

	@Override
	public void paint(Graphics g) {
		update(g);
	}

	@Override
	public void update(Graphics graphics) {
		if(firstTime) {
			image = (BufferedImage) createImage(getWidth(), getHeight());
			g = image.createGraphics();
			firstTime = false;
		}
		/*
		 * x += 7; if(x > image.getWidth() - background.getWidth(null)) x *= -1;
		 * y += 7; if(y > image.getHeight() - background.getHeight(null)) y *=
		 * -1;
		 */
		g.setColor(new Color(192, 192, 192));
		g.fillRect(0, 0, image.getWidth(), image.getHeight());
		g.setColor(new Color(246, 234, 219));
		g.fillRect(0, 502, image.getWidth(), image.getHeight());

		g.setColor(Color.BLACK);
		g.drawRect(50, 50, 1, 1);

		paint.render(g);

		graphics.drawImage(image, 0, 0, this);
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if(arg0.getPoint().y > 502)
			return;
		x = (int) arg0.getPoint().getX();
		y = (int) arg0.getPoint().getY();
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		if(arg0.getPoint().y > 502)
			return;
		x = (int) arg0.getPoint().getX();
		y = (int) arg0.getPoint().getY();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		pressed = true;
		if(e.getPoint().y > 502)
			return;
		x = e.getPoint().x;
		y = e.getPoint().y;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		pressed = false;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
}