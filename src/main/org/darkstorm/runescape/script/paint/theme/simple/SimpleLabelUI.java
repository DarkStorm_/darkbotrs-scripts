package org.darkstorm.runescape.script.paint.theme.simple;

import java.awt.*;

import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.component.Label;
import org.darkstorm.runescape.script.paint.theme.AbstractComponentUI;

public class SimpleLabelUI extends AbstractComponentUI<Label> {
	private final SimpleTheme theme;

	SimpleLabelUI(SimpleTheme theme) {
		super(Label.class);
		this.theme = theme;

		foreground = Color.WHITE;
		background = new Color(128, 128, 128, 128);
	}

	@Override
	protected void renderComponent(Label label, Graphics2D g) {
		translateComponent(g, label, false);
		g.setFont(label.getFont());
		FontMetrics metrics = g.getFontMetrics();

		int x = 0, y = 0;
		switch(label.getHorizontalAlignment()) {
		case CENTER:
			x += label.getWidth() / 2 - metrics.stringWidth(label.getText())
					/ 2;
			break;
		case RIGHT:
			x += label.getWidth() - metrics.stringWidth(label.getText()) - 2;
			break;
		default:
			x += 2;
		}
		switch(label.getVerticalAlignment()) {
		case TOP:
			y += 2;
			break;
		case BOTTOM:
			y += label.getHeight() - metrics.getHeight() - 2;
			break;
		default:
			y += label.getHeight() / 2 - metrics.getHeight() / 2;
		}

		g.setColor(label.getForegroundColor());
		g.drawString(label.getText(), x, y + metrics.getAscent());

		translateComponent(g, label, true);
	}

	@Override
	public Font getDefaultFont(Component component) {
		return theme.getFont();
	}

	@Override
	protected Dimension getDefaultComponentSize(Label component) {
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());
		return new Dimension(metrics.stringWidth(component.getText()) + 4,
				metrics.getHeight());
	}
}