package org.darkstorm.runescape.script.paint.theme.simple;

import java.awt.*;

import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.component.Panel;
import org.darkstorm.runescape.script.paint.layout.Constraint;
import org.darkstorm.runescape.script.paint.theme.AbstractComponentUI;

public class SimplePanelUI extends AbstractComponentUI<Panel> {
	private final SimpleTheme theme;

	SimplePanelUI(SimpleTheme theme) {
		super(Panel.class);
		this.theme = theme;

		foreground = Color.WHITE;
		background = new Color(128, 128, 128, 128);
	}

	@Override
	protected void renderComponent(Panel component, Graphics2D g) {
		if(component.getParent() != null)
			return;
		Rectangle area = component.getArea();
		translateComponent(g, component, false);

		g.setColor(component.getBackgroundColor());
		g.fillRect(0, 0, area.width, area.height);

		translateComponent(g, component, true);
	}

	@Override
	public Font getDefaultFont(Component component) {
		return theme.getFont();
	}

	@Override
	protected Dimension getDefaultComponentSize(Panel component) {
		Component[] children = component.getChildren();
		Rectangle[] areas = new Rectangle[children.length];
		Constraint[][] constraints = new Constraint[children.length][];
		for(int i = 0; i < children.length; i++) {
			Component child = children[i];
			Dimension size = child.getTheme().getUIForComponent(child)
					.getDefaultSize(child);
			areas[i] = new Rectangle(0, 0, size.width, size.height);
			constraints[i] = component.getConstraints(child);
		}
		return component.getLayoutManager().getOptimalPositionedSize(areas,
				constraints);
	}
}