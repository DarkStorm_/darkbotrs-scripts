package org.darkstorm.runescape.script.paint.theme.simple;

import java.awt.*;
import java.awt.event.MouseEvent;

import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.component.*;
import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.theme.AbstractComponentUI;

public class SimpleComboBoxUI extends AbstractComponentUI<ComboBox> {
	private final SimpleTheme theme;

	SimpleComboBoxUI(SimpleTheme theme) {
		super(ComboBox.class);
		this.theme = theme;

		foreground = Color.WHITE;
		background = new Color(128, 128, 128, 128 + 128 / 2);
	}

	@Override
	protected void renderComponent(ComboBox component, Graphics2D g) {
		translateComponent(g, component, false);
		Rectangle area = component.getArea();
		g.setFont(component.getFont());
		FontMetrics metrics = g.getFontMetrics();

		int maxWidth = 0;
		for(String element : component.getElements())
			maxWidth = Math.max(maxWidth, metrics.stringWidth(element));
		int extendedHeight = 0;
		if(component.isSelected()) {
			String[] elements = component.getElements();
			for(int i = 0; i < elements.length - 1; i++)
				extendedHeight += metrics.getHeight() + 2;
			extendedHeight += 2;
		}

		g.setColor(component.getBackgroundColor());
		g.fillRect(0, 0, area.width, area.height + extendedHeight);
		Point mouse = component.getPaint().getMouseLocation();
		Component parent = component.getParent();
		while(parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}
		int opacity = (int) ((component.getPaint().isButtonPressed(
				MouseEvent.BUTTON1) ? 0.5 : 0.3) * 255);
		g.setColor(new Color(0, 0, 0, opacity));
		if(area.contains(mouse)) {
			g.fillRect(0, 0, area.width, area.height);
		} else if(component.isSelected() && mouse.x >= area.x
				&& mouse.x <= area.x + area.width) {
			int offset = component.getHeight();
			String[] elements = component.getElements();
			for(int i = 0; i < elements.length; i++) {
				if(i == component.getSelectedIndex())
					continue;
				int height = metrics.getHeight() + 2;
				if((component.getSelectedIndex() == 0 ? i == 1 : i == 0)
						|| (component.getSelectedIndex() == elements.length - 1 ? i == elements.length - 2
								: i == elements.length - 1))
					height++;
				if(mouse.y >= area.y + offset
						&& mouse.y <= area.y + offset + height) {
					g.fillRect(0, offset, area.width, height);
					break;
				}
				offset += height;
			}
		}
		int height = metrics.getHeight() + 4;

		int[] xPoints = new int[3], yPoints = new int[3];
		if(component.isSelected()) {
			xPoints[0] = (int) (maxWidth + 4 + height / 2d);
			yPoints[0] = (int) (height / 3d);

			xPoints[1] = (int) (maxWidth + 4 + height / 3d);
			yPoints[1] = (int) (2d * height / 3d);

			xPoints[2] = (int) (maxWidth + 4 + 2d * height / 3d);
			yPoints[2] = (int) (2d * height / 3d);
		} else {
			xPoints[0] = (int) (maxWidth + 4 + height / 3d);
			yPoints[0] = (int) (height / 3d);

			xPoints[1] = (int) (maxWidth + 4 + 2d * height / 3d);
			yPoints[1] = (int) (height / 3d);

			xPoints[2] = (int) (maxWidth + 4 + height / 2d);
			yPoints[2] = (int) (2d * height / 3d);
		}
		g.setColor(new Color(0, 0, 0, (int) (0.3 * 255)));
		g.fillPolygon(xPoints, yPoints, 3);
		g.setColor(Color.BLACK);
		g.drawPolygon(xPoints, yPoints, 3);

		if(component.isSelected())
			g.drawLine(2, area.height, area.width - 4, area.height);
		g.drawLine(maxWidth + 4, 2, maxWidth + 4, area.height - 4);

		String text = component.getSelectedElement();
		g.setColor(component.getForegroundColor());
		g.drawString(text, 2, area.height / 2 - metrics.getHeight() / 2
				+ metrics.getAscent());
		if(component.isSelected()) {
			int offset = area.height + 2;
			String[] elements = component.getElements();
			for(int i = 0; i < elements.length; i++) {
				if(i == component.getSelectedIndex())
					continue;
				g.drawString(elements[i], 2, offset + metrics.getAscent());
				offset += metrics.getHeight() + 2;
			}
		}

		translateComponent(g, component, true);
	}

	@Override
	public Font getDefaultFont(Component component) {
		return theme.getFont();
	}

	@Override
	protected Dimension getDefaultComponentSize(ComboBox component) {
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());
		int maxWidth = 0;
		for(String element : component.getElements())
			maxWidth = Math.max(maxWidth, metrics.stringWidth(element));
		return new Dimension(maxWidth + 8 + metrics.getHeight(),
				metrics.getHeight() + 4);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(ComboBox component) {
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());
		int height = component.getHeight();
		if(component.isSelected()) {
			String[] elements = component.getElements();
			for(int i = 0; i < elements.length; i++)
				height += metrics.getHeight() + 2;
			height += 2;
		}
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				height) };
	}

	@Override
	protected void handleComponentInteraction(ComboBox component,
			Point location, int button) {
		if(button != 0)
			return;
		if(location.x <= component.getWidth()
				&& location.y <= component.getHeight())
			component.setSelected(!component.isSelected());
		else if(location.x <= component.getWidth() && component.isSelected()) {
			Paint paint = component.getPaint();
			Graphics g = paint.getGraphics();
			FontMetrics metrics = g.getFontMetrics(theme.getFont());
			int offset = component.getHeight() + 2;
			String[] elements = component.getElements();
			for(int i = 0; i < elements.length; i++) {
				if(i == component.getSelectedIndex())
					continue;
				if(location.y >= offset
						&& location.y <= offset + metrics.getHeight()) {
					component.setSelectedIndex(i);
					component.setSelected(false);
					break;
				}
				offset += metrics.getHeight() + 2;
			}
		}
	}
}