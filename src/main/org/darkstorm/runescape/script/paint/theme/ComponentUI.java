package org.darkstorm.runescape.script.paint.theme;

import java.awt.*;

import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.component.Container;

public interface ComponentUI {
	public void render(Component component, Graphics2D graphics);

	public Rectangle getChildRenderArea(Container container);

	public Dimension getDefaultSize(Component component);

	public Color getDefaultBackgroundColor(Component component);

	public Color getDefaultForegroundColor(Component component);

	public Font getDefaultFont(Component component);

	public Rectangle[] getInteractableRegions(Component component);

	public void handleInteraction(Component component, Point location,
			int button);
}
