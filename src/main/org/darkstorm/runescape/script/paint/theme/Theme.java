package org.darkstorm.runescape.script.paint.theme;

import org.darkstorm.runescape.script.paint.component.Component;

public interface Theme {
	public ComponentUI getUIForComponent(Component component);
}
