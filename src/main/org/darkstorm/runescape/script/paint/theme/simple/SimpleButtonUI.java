package org.darkstorm.runescape.script.paint.theme.simple;

import java.awt.*;
import java.awt.event.MouseEvent;

import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.component.Button;
import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.theme.AbstractComponentUI;

public class SimpleButtonUI extends AbstractComponentUI<Button> {
	private final SimpleTheme theme;

	SimpleButtonUI(SimpleTheme theme) {
		super(Button.class);
		this.theme = theme;

		foreground = Color.WHITE;
		background = new Color(128, 128, 128, 128 + 128 / 2);
	}

	@Override
	protected void renderComponent(Button button, Graphics2D graphics) {
		Paint paint = button.getPaint();
		translateComponent(graphics, button, false);
		Rectangle area = button.getArea();

		graphics.setColor(button.getBackgroundColor());
		graphics.fillRect(0, 0, area.width, area.height);

		Point mouse = paint.getMouseLocation();
		Component parent = button.getParent();
		while(parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}
		if(area.contains(mouse)) {
			int opacity = (int) ((paint.isButtonPressed(MouseEvent.BUTTON1) ? 0.5
					: 0.3) * 255);
			graphics.setColor(new Color(0, 0, 0, opacity));
			graphics.fillRect(0, 0, area.width, area.height);
		}

		String text = button.getText();
		graphics.setColor(button.getForegroundColor());
		graphics.setFont(button.getFont());
		FontMetrics metrics = graphics.getFontMetrics();
		graphics.drawString(text, area.width / 2 - metrics.stringWidth(text)
				/ 2,
				area.height / 2 - metrics.getHeight() / 2 + metrics.getAscent());

		translateComponent(graphics, button, true);
	}

	@Override
	public Font getDefaultFont(Component component) {
		return theme.getFont();
	}

	@Override
	protected Dimension getDefaultComponentSize(Button component) {
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());
		return new Dimension(metrics.stringWidth(component.getText()) + 4,
				metrics.getHeight() + 4);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(Button component) {
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				component.getHeight()) };
	}

	@Override
	protected void handleComponentInteraction(Button component, Point location,
			int button) {
		if(location.x <= component.getWidth()
				&& location.y <= component.getHeight() && button == 0)
			component.press();
	}
}