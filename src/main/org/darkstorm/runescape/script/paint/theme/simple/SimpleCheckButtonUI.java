package org.darkstorm.runescape.script.paint.theme.simple;

import java.awt.*;
import java.awt.event.MouseEvent;

import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.component.*;
import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.theme.AbstractComponentUI;

public class SimpleCheckButtonUI extends AbstractComponentUI<CheckButton> {
	private final SimpleTheme theme;

	SimpleCheckButtonUI(SimpleTheme theme) {
		super(CheckButton.class);
		this.theme = theme;

		foreground = Color.WHITE;
		background = new Color(128, 128, 128, 128 + 128 / 2);
	}

	@Override
	protected void renderComponent(CheckButton button, Graphics2D g) {
		translateComponent(g, button, false);
		Rectangle area = button.getArea();

		g.setColor(button.getBackgroundColor());
		int size = area.height - 4;
		g.fillRect(2, 2, size, size);

		if(button.isSelected()) {
			g.setColor(new Color(0, 0, 0, 128));
			g.fillRect(3, 3, size - 1, size - 1);
		}
		g.setColor(Color.BLACK);
		g.drawRect(2, 2, size, size);
		Point mouse = button.getPaint().getMouseLocation();
		Component parent = button.getParent();
		while(parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}
		if(area.contains(mouse)) {
			int opacity = (int) ((button.getPaint().isButtonPressed(
					MouseEvent.BUTTON1) ? 0.5 : 0.3) * 255);
			g.setColor(new Color(0, 0, 0, opacity));
			g.fillRect(0, 0, area.width, area.height);
		}

		String text = button.getText();
		g.setColor(button.getForegroundColor());
		g.setFont(button.getFont());
		FontMetrics metrics = g.getFontMetrics();
		g.drawString(text, size + 4, area.height / 2 - metrics.getHeight() / 2
				+ metrics.getAscent());

		translateComponent(g, button, true);
	}

	@Override
	public Font getDefaultFont(Component component) {
		return theme.getFont();
	}

	@Override
	protected Dimension getDefaultComponentSize(CheckButton component) {
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());
		return new Dimension(metrics.stringWidth(component.getText())
				+ metrics.getHeight() + 6, metrics.getHeight() + 4);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(CheckButton component) {
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				component.getHeight()) };
	}

	@Override
	protected void handleComponentInteraction(CheckButton component,
			Point location, int button) {
		if(location.x <= component.getWidth()
				&& location.y <= component.getHeight() && button == 0)
			component.press();
	}
}