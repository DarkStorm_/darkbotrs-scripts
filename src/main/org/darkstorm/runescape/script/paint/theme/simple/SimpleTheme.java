package org.darkstorm.runescape.script.paint.theme.simple;

import java.awt.Font;

import org.darkstorm.runescape.script.paint.theme.AbstractTheme;

public class SimpleTheme extends AbstractTheme {
	private final Font font;

	public SimpleTheme() {
		font = new Font("Arial", Font.PLAIN, 15);

		installUI(new SimpleFrameUI(this));
		installUI(new SimplePanelUI(this));
		installUI(new SimpleLabelUI(this));
		installUI(new SimpleButtonUI(this));
		installUI(new SimpleCheckButtonUI(this));
		installUI(new SimpleComboBoxUI(this));
	}

	public Font getFont() {
		return font;
	}
}
