package org.darkstorm.runescape.script.paint.theme.simple;

import java.awt.*;

import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.component.Frame;
import org.darkstorm.runescape.script.paint.layout.Constraint;
import org.darkstorm.runescape.script.paint.theme.AbstractComponentUI;

public class SimpleFrameUI extends AbstractComponentUI<Frame> {
	private final SimpleTheme theme;

	SimpleFrameUI(SimpleTheme theme) {
		super(Frame.class);
		this.theme = theme;

		foreground = Color.WHITE;
		background = new Color(128, 128, 128, 128);
	}

	@Override
	protected void renderComponent(Frame component, Graphics2D g) {
		Rectangle area = new Rectangle(component.getArea());
		g.setFont(component.getFont());
		FontMetrics metrics = g.getFontMetrics();
		int fontHeight = metrics.getHeight();
		translateComponent(g, component, false);

		// Draw frame background
		if(component.isMinimized())
			area.height = fontHeight + 4;
		g.setColor(component.getBackgroundColor());
		g.fillRect(0, 0, area.width, area.height);

		// Draw controls
		int offset = component.getWidth() - 2;
		Point mouse = component.getPaint().getMouseLocation();
		Component parent = component;
		while(parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}
		boolean[] checks = new boolean[] { component.isClosable(),
				component.isPinnable(), component.isMinimizable() };
		boolean[] overlays = new boolean[] { false, component.isPinned(),
				component.isMinimized() };
		for(int i = 0; i < checks.length; i++) {
			if(!checks[i])
				continue;
			g.setColor(component.getBackgroundColor());
			g.fillRect(offset - fontHeight, 2, fontHeight, fontHeight);
			if(overlays[i]) {
				g.setColor(new Color(0, 0, 0, 128));
				g.fillRect(offset - fontHeight, 2, fontHeight, fontHeight);
			}
			if(mouse.x >= offset - fontHeight && mouse.x <= offset
					&& mouse.y >= 2 && mouse.y <= fontHeight + 2) {
				g.setColor(new Color(0, 0, 0, (int) (0.3 * 255)));
				g.fillRect(offset - fontHeight, 2, fontHeight, fontHeight);
			}
			g.setColor(Color.BLACK);
			g.drawRect(offset - fontHeight, 2, fontHeight, fontHeight);
			offset -= fontHeight + 2;
		}

		g.setColor(Color.BLACK);
		g.drawLine(2, fontHeight + 4, area.width - 4, fontHeight + 4);

		g.setColor(component.getForegroundColor());
		g.drawString(component.getTitle(), 2, 2 + metrics.getHeight());

		translateComponent(g, component, true);
	}

	@Override
	public Font getDefaultFont(Component component) {
		return theme.getFont().deriveFont(Font.BOLD);
	}

	@Override
	protected Rectangle getContainerChildRenderArea(Frame container) {
		Paint paint = container.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());

		Rectangle area = new Rectangle(container.getArea());
		area.x = 2;
		area.y = metrics.getHeight() + 6;
		area.width -= 4;
		area.height -= metrics.getHeight() + 8;
		return area;
	}

	@Override
	protected Dimension getDefaultComponentSize(Frame component) {
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());

		Component[] children = component.getChildren();
		Rectangle[] areas = new Rectangle[children.length];
		Constraint[][] constraints = new Constraint[children.length][];
		for(int i = 0; i < children.length; i++) {
			Component child = children[i];
			Dimension size = child.getTheme().getUIForComponent(child)
					.getDefaultSize(child);
			areas[i] = new Rectangle(0, 0, size.width, size.height);
			constraints[i] = component.getConstraints(child);
		}
		Dimension size = component.getLayoutManager().getOptimalPositionedSize(
				areas, constraints);
		size.width += 4;
		size.height += metrics.getHeight() + 8;
		return size;
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(Frame component) {
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				metrics.getHeight() + 4) };
	}

	@Override
	protected void handleComponentInteraction(Frame component, Point location,
			int button) {
		if(button != 0)
			return;
		Paint paint = component.getPaint();
		Graphics g = paint.getGraphics();
		FontMetrics metrics = g.getFontMetrics(theme.getFont());
		int offset = component.getWidth() - 2;
		int textHeight = metrics.getHeight();
		if(component.isClosable()) {
			if(location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.close();
				return;
			}
			offset -= textHeight + 2;
		}
		if(component.isPinnable()) {
			if(location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.setPinned(!component.isPinned());
				return;
			}
			offset -= textHeight + 2;
		}
		if(component.isMinimizable()) {
			if(location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.setMinimized(!component.isMinimized());
				return;
			}
			offset -= textHeight + 2;
		}
		if(location.x >= 0 && location.x <= offset && location.y >= 0
				&& location.y <= textHeight + 4) {
			component.setDragging(true);
			return;
		}
	}
}