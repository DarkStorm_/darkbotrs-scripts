package org.darkstorm.runescape.script.paint;

import java.awt.Graphics;

import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.theme.Theme;

public interface Paint {
	public void addComponent(Component component);

	public void removeComponent(Component component);

	public Component[] getComponents();

	public void bringForward(Component component);

	public Theme getTheme();

	public void setTheme(Theme theme);

	public void render(java.awt.Graphics2D graphics);

	public java.awt.Point getMouseLocation();

	public boolean isButtonPressed(int button);

	public java.awt.Component getSource();

	public Graphics getGraphics();

	public void dispose();
}
