package org.darkstorm.runescape.script.paint.component;

import org.darkstorm.runescape.script.paint.listener.ButtonListener;

public interface Button extends Component, TextComponent {
	public void press();

	public void addButtonListener(ButtonListener listener);

	public void removeButtonListener(ButtonListener listener);

	public ButtonGroup getGroup();

	public void setGroup(ButtonGroup group);
}
