package org.darkstorm.runescape.script.paint.component;

public interface ButtonGroup {
	public void addButton(Button button);

	public void removeButton(Button button);

	public Button[] getButtons();
}
