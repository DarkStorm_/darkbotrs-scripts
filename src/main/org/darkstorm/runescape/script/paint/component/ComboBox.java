package org.darkstorm.runescape.script.paint.component;

import org.darkstorm.runescape.script.paint.listener.ComboBoxListener;

public interface ComboBox extends Component, SelectableComponent {
	public String[] getElements();

	public void setElements(String... elements);

	public int getSelectedIndex();

	public void setSelectedIndex(int selectedIndex);

	public String getSelectedElement();

	public void addComboBoxListener(ComboBoxListener listener);

	public void removeComboBoxListener(ComboBoxListener listener);
}
