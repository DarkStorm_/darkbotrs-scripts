package org.darkstorm.runescape.script.paint.component;

import java.awt.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.listener.ComponentListener;
import org.darkstorm.runescape.script.paint.theme.*;

public abstract class AbstractComponent implements Component {
	private Container parent = null;
	private Theme theme;
	private Paint paint;

	protected Rectangle area = new Rectangle(0, 0, 0, 0);
	protected ComponentUI ui;
	protected Color foreground, background;
	protected Font font;
	protected boolean enabled = true, visible = true;

	private List<ComponentListener> listeners = new CopyOnWriteArrayList<ComponentListener>();

	@Override
	public void render(Graphics2D graphics) {
		if(ui == null)
			return;
		ui.render(this, graphics);
	}

	@Override
	public void update() {
	}

	protected ComponentUI getUI() {
		return theme.getUIForComponent(this);
	}

	@Override
	public void onMousePress(int x, int y, int button) {
		if(ui != null) {
			for(Rectangle area : ui.getInteractableRegions(this)) {
				if(area.contains(x, y)) {
					ui.handleInteraction(this, new Point(x, y), button);
					break;
				}
			}
		}
	}

	@Override
	public void onMouseRelease(int x, int y, int button) {
	}

	@Override
	public void onMouseMove(int x, int y) {
	}

	@Override
	public void onMouseDrag(int x, int y, int button) {
	}

	@Override
	public Theme getTheme() {
		return theme;
	}

	@Override
	public void setTheme(Theme theme) {
		Theme oldTheme = this.theme;
		this.theme = theme;
		if(theme == null) {
			ui = null;
			foreground = null;
			background = null;
			return;
		}

		ui = getUI();
		boolean changeArea;
		if(oldTheme != null) {
			Dimension defaultSize = oldTheme.getUIForComponent(this)
					.getDefaultSize(this);
			changeArea = area.width == defaultSize.width
					&& area.height == defaultSize.height;
		} else
			changeArea = area.equals(new Rectangle(0, 0, 0, 0));
		if(changeArea) {
			Dimension defaultSize = ui.getDefaultSize(this);
			area = new Rectangle(area.x, area.y, defaultSize.width,
					defaultSize.height);
		}
		if(foreground == null
				|| (oldTheme != null && foreground.equals(oldTheme
						.getUIForComponent(this)
						.getDefaultForegroundColor(this))))
			foreground = ui.getDefaultForegroundColor(this);
		if(background == null
				|| (oldTheme != null && background.equals(oldTheme
						.getUIForComponent(this)
						.getDefaultBackgroundColor(this))))
			background = ui.getDefaultBackgroundColor(this);
		if(font == null
				|| (oldTheme != null && font.equals(oldTheme.getUIForComponent(
						this).getDefaultFont(this))))
			font = ui.getDefaultFont(this);
	}

	@Override
	public int getX() {
		return area.x;
	}

	@Override
	public int getY() {
		return area.y;
	}

	@Override
	public int getWidth() {
		return area.width;
	}

	@Override
	public int getHeight() {
		return area.height;
	}

	@Override
	public void setX(int x) {
		area.x = x;
	}

	@Override
	public void setY(int y) {
		area.y = y;
	}

	@Override
	public void setWidth(int width) {
		area.width = width;
	}

	@Override
	public void setHeight(int height) {
		area.height = height;
	}

	@Override
	public Color getBackgroundColor() {
		return background;
	}

	@Override
	public Color getForegroundColor() {
		return foreground;
	}

	@Override
	public void setBackgroundColor(Color color) {
		background = color;
	}

	@Override
	public void setForegroundColor(Color color) {
		foreground = color;
	}

	@Override
	public Font getFont() {
		return font;
	}

	@Override
	public void setFont(Font font) {
		this.font = font;
	}

	@Override
	public Point getLocation() {
		return area.getLocation();
	}

	@Override
	public Dimension getSize() {
		return area.getSize();
	}

	@Override
	public Rectangle getArea() {
		return area;
	}

	@Override
	public Container getParent() {
		return parent;
	}

	@Override
	public void setParent(Container parent) {
		if(!parent.hasChild(this)
				|| (this.parent != null && this.parent.hasChild(this)))
			throw new IllegalArgumentException();
		this.parent = parent;
	}

	@Override
	public void resize() {
		Dimension defaultDimension = ui.getDefaultSize(this);
		setWidth(defaultDimension.width);
		setHeight(defaultDimension.height);
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		if(parent != null && !parent.isEnabled())
			this.enabled = false;
		else
			this.enabled = enabled;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(boolean visible) {
		if(parent != null && !parent.isVisible())
			this.visible = false;
		else
			this.visible = visible;
	}

	protected void addListener(ComponentListener listener) {
		synchronized(listeners) {
			listeners.add(listener);
		}
	}

	protected void removeListener(ComponentListener listener) {
		synchronized(listeners) {
			listeners.remove(listener);
		}
	}

	protected ComponentListener[] getListeners() {
		synchronized(listeners) {
			return listeners.toArray(new ComponentListener[listeners.size()]);
		}
	}

	@Override
	public Paint getPaint() {
		return paint;
	}

	@Override
	public void setPaint(Paint paint) {
		this.paint = paint;
	}
}
