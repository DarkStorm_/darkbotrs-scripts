package org.darkstorm.runescape.script.paint.component;


public interface TextComponent extends Component {
	public String getText();

	public void setText(String text);
}
