package org.darkstorm.runescape.script.paint.component;


public interface DraggableComponent extends Component {
	public boolean isDragging();

	public void setDragging(boolean dragging);
}
