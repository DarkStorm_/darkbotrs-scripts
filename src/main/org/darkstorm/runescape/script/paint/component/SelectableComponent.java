package org.darkstorm.runescape.script.paint.component;

import org.darkstorm.runescape.script.paint.listener.SelectableComponentListener;

public interface SelectableComponent extends Component {
	public boolean isSelected();

	public void setSelected(boolean selected);

	public void addSelectableComponentListener(SelectableComponentListener listener);

	public void removeSelectableComponentListener(SelectableComponentListener listener);
}
