package org.darkstorm.runescape.script.paint.component;

public interface Label extends TextComponent {
	public enum TextAlignment {
		CENTER,
		LEFT,
		RIGHT,
		TOP,
		BOTTOM
	}

	public TextAlignment getHorizontalAlignment();

	public TextAlignment getVerticalAlignment();

	public void setHorizontalAlignment(TextAlignment alignment);

	public void setVerticalAlignment(TextAlignment alignment);
}
