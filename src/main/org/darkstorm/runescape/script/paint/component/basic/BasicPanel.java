package org.darkstorm.runescape.script.paint.component.basic;

import org.darkstorm.runescape.script.paint.component.*;
import org.darkstorm.runescape.script.paint.layout.LayoutManager;

public class BasicPanel extends AbstractContainer implements Panel {
	public BasicPanel() {
	}

	public BasicPanel(LayoutManager layoutManager) {
		setLayoutManager(layoutManager);
	}
}
