package org.darkstorm.runescape.script.util;

import java.util.concurrent.TimeUnit;
import java.util.regex.*;

public final class Util {
	private static final Pattern timePattern = Pattern
			.compile("(?i)(?<d>[0-9]+d)?(?<h>[0-9]+h)?(?<m>[0-9]+m)?(?<s>[0-9]+s)?(?<ms>[0-9]+ms)?");
	private static final Pattern numberPattern = Pattern.compile("[0-9]+");

	private Util() {
		throw new UnsupportedOperationException();
	}

	public static long parseTime(String value) {
		Matcher matcher = timePattern.matcher(value);
		if(value.isEmpty() || !matcher.matches())
			throw new IllegalArgumentException("Invalid value");
		long millis = 0;
		millis += toMillis(matcher.group("d"), TimeUnit.DAYS);
		millis += toMillis(matcher.group("h"), TimeUnit.HOURS);
		millis += toMillis(matcher.group("m"), TimeUnit.MINUTES);
		millis += toMillis(matcher.group("s"), TimeUnit.SECONDS);
		millis += toMillis(matcher.group("ms"), TimeUnit.MILLISECONDS);
		return millis;
	}

	private static long toMillis(String value, TimeUnit unit) {
		if(value != null && !value.isEmpty()) {
			Matcher matcher = numberPattern.matcher(value);
			if(!matcher.find())
				return 0;
			return unit.toMillis(Integer.parseInt(matcher.group()));
		}
		return 0;
	}

	public static String toString(Object[] objects) {
		return toString(objects, ", ", "");
	}

	public static String toString(Object[] objects, String separator) {
		return toString(objects, separator, "");
	}

	public static String toString(Object[] objects, String separator,
			String finalSeparator) {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < objects.length; i++) {
			if(i != 0)
				builder.append(separator);
			if(i == objects.length - 1)
				builder.append(finalSeparator);
			builder.append(objects[i]);
		}
		return builder.toString();
	}

	public static String toStringLong(long time) {
		long hours = time / 60 / 60 / 1000;
		long minutes = (time / 60 / 1000) - (hours * 60);
		long seconds = (time / 1000) - (minutes * 60) - (hours * 60 * 60);
		long milliseconds = time - (seconds * 1000) - (minutes * 60 * 1000)
				- (hours * 60 * 60 * 1000);

		StringBuilder builder = new StringBuilder();
		if(hours > 0) {
			builder.append(hours).append(" hour");
			if(hours != 1)
				builder.append('s');
			if(minutes > 0 || seconds > 0 || milliseconds > 0)
				builder.append(", ");
		}
		if(minutes > 0) {
			builder.append(minutes).append(" minute");
			if(minutes != 1)
				builder.append('s');
			if(seconds > 0 || milliseconds > 0)
				builder.append(", ");
		}
		if(seconds > 0) {
			builder.append(seconds).append(" second");
			if(seconds != 1)
				builder.append('s');
			if(milliseconds > 0)
				builder.append(", ");
		}
		if(milliseconds > 0) {
			builder.append(milliseconds).append(" millisecond");
			if(milliseconds != 1)
				builder.append('s');
		}
		return builder.toString();
	}

	public static String toStringShort(long time) {
		long hours = time / 60 / 60 / 1000;
		long minutes = (time / 60 / 1000) - (hours * 60);
		long seconds = (time / 1000) - (minutes * 60) - (hours * 60 * 60);
		long milliseconds = time - (seconds * 1000) - (minutes * 60 * 1000)
				- (hours * 60 * 60 * 1000);

		StringBuilder builder = new StringBuilder();
		if(hours < 10)
			builder.append('0');
		builder.append(hours).append(':');
		if(minutes < 10)
			builder.append('0');
		builder.append(minutes).append(':');
		if(seconds < 10)
			builder.append('0');
		builder.append(seconds).append(':');
		if(milliseconds < 10)
			builder.append('0');
		builder.append(milliseconds);
		return builder.toString();
	}
}
