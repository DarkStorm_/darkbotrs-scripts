package org.darkstorm.runescape.script.util;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;

import javax.imageio.ImageIO;
import javax.swing.*;

public final class GrandExchange {
	private static final String GE_URL = "http://services.runescape.com/m=itemdb_rs/";
	private static final String GE_SEARCH_URL = "http://services.runescape.com/m=itemdb_rs/results.ws?query=%s&price=%s&members=%s&page=%d";
	private static final String LARGE_ICON_URL;
	private static final String SMALL_ICON_URL;

	private GrandExchange() {
		throw new IllegalArgumentException();
	}

	public static String getLargeItemImageURL(int itemId) {
		return String.format(LARGE_ICON_URL, itemId);
	}

	public static String getSmallItemImageURL(int itemId) {
		return String.format(SMALL_ICON_URL, itemId);
	}

	public static Icon getLargeItemImage(int itemId) throws IOException {
		return new ImageIcon(ImageIO.read(new URL(String.format(LARGE_ICON_URL,
				itemId))));
	}

	public static Icon getSmallItemImage(int itemId) throws IOException {
		return new ImageIcon(ImageIO.read(new URL(String.format(SMALL_ICON_URL,
				itemId))));
	}

	public static SearchResult[] search(String name) throws IOException {
		return search(name, Integer.MAX_VALUE);
	}

	public static SearchResult[] search(String name, int maxPages)
			throws IOException {
		Pattern pagePattern = Pattern
				.compile("<div class=\"res-displaying\">Displaying <em>[0-9]+-([0-9]+)</em> of <em>([0-9]+)</em> results</div>");
		Pattern itemPattern = Pattern.compile("<tr data-item-id=\"([0-9]+)\">");
		Pattern namePattern = Pattern
				.compile("<a href=\"(http://services\\.runescape\\.com/m=itemdb_rs/.*/viewitem\\.ws\\?obj=[0-9]+)\">([^<]+)</a>");
		Pattern pricePattern = Pattern
				.compile("<td class=\"price\">([0-9,]+)</td>");
		Pattern changePattern = Pattern
				.compile("<td class=\"(neutral|positive|negative)\">[+]?([-]?[0-9]+)</td>");
		Pattern membersPattern = Pattern
				.compile("<img src=\"http://www\\.runescape\\.com/img/itemdb/members-icon\\.png\" alt=\"Members' only item\" title=\"Members' only item\">");

		List<SearchResult> results = new ArrayList<>();
		int page = 0;
		int pages = 1;
		while(page < pages) {
			page++;
			String url = String.format(GE_SEARCH_URL,
					URLEncoder.encode(name, "UTF-8"), "all", "", page);

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new URL(url).openStream()));
			try {
				SearchResult.Builder builder = null;

				String line;
				while((line = reader.readLine()) != null) {
					Matcher pageMatcher = pagePattern.matcher(line);
					if(pageMatcher.matches()
							&& Integer.parseInt(pageMatcher.group(1)) != Integer
									.parseInt(pageMatcher.group(2))
							&& pages < maxPages)
						pages++;
					Matcher itemMatcher = itemPattern.matcher(line);
					if(itemMatcher.matches()) {
						if(builder != null)
							results.add(builder.build());
						builder = SearchResult.builder(Integer
								.parseInt(itemMatcher.group(1)));
						continue;
					}
					Matcher nameMatcher = namePattern.matcher(line);
					if(nameMatcher.matches()) {
						builder.withUrl(nameMatcher.group(1));
						builder.withName(nameMatcher.group(2));
					}
					Matcher priceMatcher = pricePattern.matcher(line);
					if(priceMatcher.matches())
						builder.withPrice(Integer.parseInt(priceMatcher
								.group(1).replace(",", "")));
					Matcher changeMatcher = changePattern.matcher(line);
					if(changeMatcher.matches())
						builder.withChange(Integer.parseInt(changeMatcher
								.group(2)));
					if(builder != null && builder.getPrice() != 0) {
						Matcher membersMatcher = membersPattern.matcher(line);
						if(membersMatcher.matches())
							builder.members();
					}
				}
				if(builder != null)
					results.add(builder.build());
			} finally {
				reader.close();
			}
		}
		return results.toArray(new SearchResult[results.size()]);
	}

	static {
		String page = readPage(GE_URL);
		int baseIconId = 4052;
		if(page != null) {
			Pattern iconPattern = Pattern
					.compile("http://services\\.runescape\\.com/m=itemdb_rs/([0-9]+)_obj_big\\.gif\\?id=");
			Matcher matcher = iconPattern.matcher(page);
			if(matcher.find())
				baseIconId = Integer.parseInt(matcher.group(1));
		}
		LARGE_ICON_URL = String.format(
				"http://services.runescape.com/m=itemdb_rs/%d_obj_big.gif?id=",
				baseIconId)
				+ "%d";
		SMALL_ICON_URL = String
				.format("http://services.runescape.com/m=itemdb_rs/%d_obj_sprite.gif?id=",
						baseIconId)
				+ "%d";
	}

	private static String readPage(String s) {
		try {
			URL url = new URL(s);
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String html = "";
			String line;
			while((line = br.readLine()) != null) {
				html += line;
			}
			return html;
		} catch(MalformedURLException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static class SearchResult {
		private final String name, url;
		private final int id, price, change;
		private final boolean members;

		private SearchResult(Builder builder) {
			name = builder.name;
			url = builder.url;
			id = builder.id;
			price = builder.price;
			change = builder.change;
			members = builder.members;
		}

		public String getName() {
			return name;
		}

		public String getUrl() {
			return url;
		}

		public int getId() {
			return id;
		}

		public int getPrice() {
			return price;
		}

		public int getChange() {
			return change;
		}

		public boolean isMembers() {
			return members;
		}

		private static Builder builder(int id) {
			return new Builder(id);
		}

		public static class Builder {
			private final int id;

			private String name, url;
			private int price, change;
			private boolean members;

			private Builder(int id) {
				this.id = id;
			}

			public Builder withName(String name) {
				this.name = name;
				return this;
			}

			public Builder withUrl(String url) {
				this.url = url;
				return this;
			}

			public Builder withPrice(int price) {
				this.price = price;
				return this;
			}

			public Builder withChange(int change) {
				this.change = change;
				return this;
			}

			public Builder members() {
				return members(true);
			}

			public Builder members(boolean members) {
				this.members = members;
				return this;
			}

			public String getName() {
				return name;
			}

			public String getUrl() {
				return url;
			}

			public int getId() {
				return id;
			}

			public int getPrice() {
				return price;
			}

			public int getChange() {
				return change;
			}

			public boolean isMembers() {
				return members;
			}

			public SearchResult build() {
				return new SearchResult(this);
			}
		}
	}
}
