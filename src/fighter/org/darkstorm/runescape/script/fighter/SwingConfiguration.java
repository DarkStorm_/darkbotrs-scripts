/*
 * Created by JFormDesigner on Sun Mar 31 14:18:14 EDT 2013
 */

package org.darkstorm.runescape.script.fighter;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.darkstorm.runescape.api.util.*;
import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.script.util.*;
import org.darkstorm.runescape.script.util.GrandExchange.SearchResult;
import org.w3c.dom.*;

/**
 * @author DarkStorm _
 */
@SuppressWarnings("serial")
class SwingConfiguration extends JDialog implements Configuration {
	private static final NamedImageData images;

	static {
		NamedImageData data = null;
		try {
			data = new NamedImageData(SwingConfiguration.class.getResource(
					"/images.dat").openStream());
		} catch(IOException e) {
			e.printStackTrace();
		}
		images = data;
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY
	// //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - DarkStorm _
	private JTabbedPane tabbedPane;
	private JLabel titleNameLabel;
	private JLabel titleVersionLabel;
	private JTextPane latestChangesPane;
	private JComboBox<String> combatComboBox;
	private JPanel combatAddPanel;
	private JTextField combatNameField;
	private JSpinner combatIdSpinner;
	private JButton combatAddButton;
	private JTable combatNearbyMonsterList;
	private JButton combatNearbyAddButton;
	private JButton combatNearbyAddAllButton;
	private JButton combatNearbyRefreshButton;
	private JButton combatRemoveButton;
	private JButton combatRemoveAllButton;
	private JCheckBox multiCombatCheckBox;
	private JLabel combatSliderLabel;
	private JSlider combatSlider;
	private JList<Monster> combatMonsterList;
	private JList<Food> unselectedFoodList;
	private JButton foodAddButton;
	private JButton foodRemoveButton;
	private JButton foodAddAllButton;
	private JButton foodRemoveAllButton;
	private JList<Food> selectedFoodList;
	private JSpinner eatLowSpinner;
	private JSpinner eatHighSpinner;
	private JComboBox<NamedEmergencyAction> outOfFoodComboBox;
	private JComboBox<NamedEmergencyAction> criticalHealthComboBox;
	private JComboBox<String> lootComboBox;
	private JPanel lootAddPanel;
	private JSpinner lootIdSpinner;
	private JTextField lootNameField;
	private JButton lootAddButton;
	private JButton lootRemoveButton;
	private JList<Loot> lootList;
	private JPanel panel1;
	private JPanel lootBonesPanel;
	private JRadioButton bonesBuryRadioButton;
	private JRadioButton bonesBananasRadioButton;
	private JRadioButton bonesPeachesRadioButton;
	private JCheckBox lootEverythingCheckBox;
	private JCheckBox equipCheckBox;
	private JComboBox<String> equipComboBox;
	private JLabel lootRadiusSliderLabel;
	private JSlider lootRadiusSlider;
	private JLabel lootDelaySliderLabel;
	private JSlider lootDelaySlider;
	private JCheckBox saveCheckBox;
	private JButton okButton;
	private JButton cancelButton;
	private JCheckBox lootBonesCheckBox;
	// JFormDesigner - End of variables declaration //GEN-END:variables

	private final DarkFighter script;
	private final IconListRenderer iconRenderer = new IconListRenderer();
	private final List<NearbyMonster> nearbyMonsters = new ArrayList<>();
	private final ListSelectionListener nearbyMonsterListener;

	private final Filter<NPC> monsterFilter;
	private final Filter<GroundItem> lootFilter;
	private final Filter<Item> foodFilter;

	private int lastFoodCount = -1;
	private int lastRandomValue = 0;

	private boolean okPressed;

	public SwingConfiguration(final DarkFighter script) {
		super(script != null ? script.getBot().getDarkBot().getFrame() : null);

		this.script = script;

		initComponents();

		Border border = ((JPanel) getContentPane().getComponent(0)).getBorder();
		((TitledBorder) ((CompoundBorder) border).getOutsideBorder())
				.setTitle("");
		Font font = new Font("Arial", Font.PLAIN, 12);
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, getClass()
					.getResourceAsStream("/neuropol x free.ttf"));
		} catch(FontFormatException e1) {
			e1.printStackTrace();
		} catch(IOException e1) {
			e1.printStackTrace();
		}
		titleNameLabel.setFont(font.deriveFont(titleNameLabel.getFont()
				.getStyle(), titleNameLabel.getFont().getSize()));
		if(script != null)
			titleVersionLabel.setText(script.getManifest().version());
		titleVersionLabel.setFont(font.deriveFont(titleVersionLabel.getFont()
				.getStyle(), titleVersionLabel.getFont().getSize()));

		DefaultListModel<Food> model = (DefaultListModel<Food>) unselectedFoodList
				.getModel();
		for(Food food : Food.values()) {
			Icon icon = food.getIcon();
			if(icon != null)
				iconRenderer.setIcon(food, icon);
			model.addElement(food);
		}

		DefaultComboBoxModel<NamedEmergencyAction> outOfFoodModel = (DefaultComboBoxModel<NamedEmergencyAction>) outOfFoodComboBox
				.getModel();
		DefaultComboBoxModel<NamedEmergencyAction> criticalHealthModel = (DefaultComboBoxModel<NamedEmergencyAction>) criticalHealthComboBox
				.getModel();
		for(EmergencyAction action : EmergencyAction.values()) {
			String[] parts = action.name().toLowerCase().split("_");
			if(parts.length > 0)
				parts[0] = java.lang.Character.toUpperCase(parts[0].charAt(0))
						+ parts[0].substring(1);
			String name = Util.toString(parts, " ");
			outOfFoodModel.addElement(new NamedEmergencyAction(action, name));
			criticalHealthModel.addElement(new NamedEmergencyAction(action,
					name));
		}

		loadConfiguration();

		combatNearbyMonsterList.getRowSorter().toggleSortOrder(2);

		nearbyMonsterListener = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateCombat();
			}
		};
		combatNearbyMonsterList.getSelectionModel().addListSelectionListener(
				nearbyMonsterListener);

		updateNearbyMonsters();

		updateCombat();
		updateFood();
		updateLoot();

		combatSliderLabel.setText(Integer.toString(combatSlider.getValue()));
		lootRadiusSliderLabel.setText(Integer.toString(lootRadiusSlider
				.getValue()));

		monsterFilter = new Filter<NPC>() {
			@Override
			public boolean accept(NPC element) {
				if((!multiCombatCheckBox.isSelected() && element.isInCombat())
						|| element.getName() == null
						|| !script.getContext().getWalking().canReach(element))
					return false;
				// if(element.getHealth() <= 0 || element.getHealthPercent() <=
				// 0)
				// return false;
				DefaultListModel<Monster> selectedModel = (DefaultListModel<Monster>) combatMonsterList
						.getModel();
				int size = selectedModel.getSize();
				for(int i = 0; i < size; i++) {
					Monster monster = selectedModel.getElementAt(i);
					if(monster instanceof NearbyMonster) {
						if(monster.id == element.getId())
							return script.getContext().getPlayers().getSelf()
									.getLocation()
									.distanceTo(element.getLocation()) < combatSlider
									.getValue();
					} else if(monster.name == null ? monster.id == element
							.getId() : element.getName().toLowerCase()
							.contains(monster.name.toLowerCase()))
						return script.getContext().getPlayers().getSelf()
								.getLocation()
								.distanceTo(element.getLocation()) < combatSlider
								.getValue();
				}
				return false;
			}
		};
		foodFilter = new Filter<Item>() {
			@Override
			public boolean accept(Item element) {
				DefaultListModel<Food> selectedModel = (DefaultListModel<Food>) selectedFoodList
						.getModel();
				int size = selectedModel.getSize();
				for(int i = 0; i < size; i++) {
					Food food = selectedModel.getElementAt(i);
					for(int id : food.getIds())
						if(id == element.getId())
							return true;
				}
				return false;
			}
		};
		lootFilter = new Filter<GroundItem>() {
			@Override
			public boolean accept(GroundItem element) {
				if(!script.getContext().getWalking().canReach(element))
					return false;
				DefaultListModel<Loot> selectedModel = (DefaultListModel<Loot>) lootList
						.getModel();
				int size = selectedModel.getSize();
				for(int i = 0; i < size; i++) {
					Loot loot = selectedModel.getElementAt(i);
					if(loot.name == null ? loot.id == element.getId() : false)
						// element.getName().toLowerCase().contains(loot.name.toLowerCase()))
						// TODO Find a fix
						return script.getContext().getPlayers().getSelf()
								.getLocation()
								.distanceTo(element.getLocation()) < lootRadiusSlider
								.getValue();
				}
				return false;
			}
		};
	}

	@Override
	public boolean awaitCompletion() {
		setVisible(true);
		return okPressed;
	}

	private void loadConfiguration() {
		String directory = ".";
		File config = new File(directory, "darkfighter.xml");
		if(!config.exists())
			return;

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			docFactory.setNamespaceAware(true);
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new FileInputStream(config));
			Element rootElement = (Element) doc.getElementsByTagName(
					"darkfighter").item(0);

			{
				Element combat = getByName(rootElement, "combat");
				Element monsters = getByName(combat, "monsters");
				NodeList monsterList = monsters.getElementsByTagName("monster");
				DefaultListModel<Monster> selectedModel = (DefaultListModel<Monster>) combatMonsterList
						.getModel();
				for(int i = 0; i < monsterList.getLength(); i++) {
					Element element = (Element) monsterList.item(i);
					Monster monster;
					if(element.hasAttribute("name"))
						monster = new Monster(element.getAttribute("name"));
					else
						monster = new Monster(Integer.parseInt(element
								.getAttribute("id")));
					selectedModel.addElement(monster);
				}
				Element multicombat = getByName(combat, "multi-combat");
				multiCombatCheckBox.setSelected(Boolean.valueOf(multicombat
						.getAttribute("value")));
				Element radius = getByName(combat, "radius");
				combatSlider.setValue(Integer.parseInt(radius
						.getAttribute("value")));
			}

			{
				Element health = getByName(rootElement, "health");
				Element food = getByName(health, "food");
				NodeList foodList = food.getElementsByTagName("item");
				DefaultListModel<Food> unselectedModel = (DefaultListModel<Food>) unselectedFoodList
						.getModel();
				DefaultListModel<Food> selectedModel = (DefaultListModel<Food>) selectedFoodList
						.getModel();
				for(int i = 0; i < foodList.getLength(); i++) {
					Element element = (Element) foodList.item(i);
					Food f = Food.valueOf(element.getAttribute("name"));
					unselectedModel.removeElement(f);
					selectedModel.addElement(f);
				}
				Element eatRange = getByName(health, "eat-range");
				int low = Integer.parseInt(eatRange.getAttribute("low"));
				int high = Integer.parseInt(eatRange.getAttribute("high"));
				SpinnerNumberModel lowModel = (SpinnerNumberModel) eatLowSpinner
						.getModel();
				lowModel.setMaximum(high);
				lowModel.setValue(low);
				SpinnerNumberModel highModel = (SpinnerNumberModel) eatHighSpinner
						.getModel();
				highModel.setMinimum(low);
				highModel.setValue(high);
				Element outOfFood = getByName(health, "out-of-food");
				EmergencyAction outOfFoodAction = EmergencyAction
						.valueOf(outOfFood.getAttribute("value"));
				DefaultComboBoxModel<NamedEmergencyAction> outOfFoodModel = (DefaultComboBoxModel<NamedEmergencyAction>) outOfFoodComboBox
						.getModel();
				for(int i = 0; i < outOfFoodModel.getSize(); i++) {
					NamedEmergencyAction action = outOfFoodModel
							.getElementAt(i);
					if(action.action == outOfFoodAction) {
						outOfFoodComboBox.setSelectedIndex(i);
						break;
					}
				}
				Element criticalHealth = getByName(health, "critical-health");
				EmergencyAction criticalHealthAction = EmergencyAction
						.valueOf(criticalHealth.getAttribute("value"));
				DefaultComboBoxModel<NamedEmergencyAction> criticalHealthModel = (DefaultComboBoxModel<NamedEmergencyAction>) criticalHealthComboBox
						.getModel();
				for(int i = 0; i < criticalHealthModel.getSize(); i++) {
					NamedEmergencyAction action = criticalHealthModel
							.getElementAt(i);
					if(action.action == criticalHealthAction) {
						criticalHealthComboBox.setSelectedIndex(i);
						break;
					}
				}
			}

			{
				Element loot = getByName(rootElement, "loot");
				Element items = getByName(loot, "items");
				NodeList itemList = items.getElementsByTagName("item");
				DefaultListModel<Loot> selectedModel = (DefaultListModel<Loot>) lootList
						.getModel();
				for(int i = 0; i < itemList.getLength(); i++) {
					Element element = (Element) itemList.item(i);
					Loot item;
					if(element.hasAttribute("name")) {
						item = new Loot(element.getAttribute("name"));
						final Loot itemFinal = item;
						iconRenderer.setLoading(itemFinal, true);
						new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									SearchResult[] results = GrandExchange
											.search(itemFinal.name, 1);
									Icon icon = null;
									if(results.length != 0) {
										SearchResult matched = null;
										for(SearchResult result : results) {
											if(result.getName() == null)
												continue;
											if(!result
													.getName()
													.toLowerCase()
													.contains(
															itemFinal.name
																	.toLowerCase()))
												continue;
											if(matched == null
													|| matched.getName()
															.length() > result
															.getName().length())
												matched = result;
										}
										if(matched == null
												&& itemFinal.name.toLowerCase()
														.contains("coins"))
											icon = images.getImage("COINS");
										if(matched != null) {
											URL url = new URL(
													GrandExchange
															.getLargeItemImageURL(matched
																	.getId()));
											BufferedImage image = ImageIO
													.read(url);
											icon = new ImageIcon(
													image.getScaledInstance(
															16,
															16,
															BufferedImage.SCALE_SMOOTH));
										}
									}

									final Icon lootIcon = icon;
									SwingUtilities.invokeLater(new Runnable() {
										@Override
										public void run() {
											iconRenderer.setLoading(itemFinal,
													false);
											iconRenderer.setIcon(itemFinal,
													lootIcon);
										}
									});
								} catch(Exception e) {
									e.printStackTrace();
									SwingUtilities.invokeLater(new Runnable() {
										@Override
										public void run() {
											iconRenderer.setLoading(itemFinal,
													false);
											iconRenderer.setIcon(itemFinal,
													null);
										}
									});
								}
							}
						}).start();
					} else {
						item = new Loot(Integer.parseInt(element
								.getAttribute("id")));
						final Loot itemFinal = item;
						iconRenderer.setLoading(itemFinal, true);
						new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									URL url = new URL(GrandExchange
											.getLargeItemImageURL(itemFinal.id));
									BufferedImage image = ImageIO.read(url);
									final Icon icon = new ImageIcon(image
											.getScaledInstance(16, 16,
													BufferedImage.SCALE_SMOOTH));
									SwingUtilities.invokeLater(new Runnable() {
										@Override
										public void run() {
											iconRenderer.setLoading(itemFinal,
													false);
											iconRenderer.setIcon(itemFinal,
													icon);
										}
									});
								} catch(Exception e) {
									SwingUtilities.invokeLater(new Runnable() {
										@Override
										public void run() {
											iconRenderer.setLoading(itemFinal,
													false);
											iconRenderer.setIcon(itemFinal,
													null);
										}
									});
								}
							}
						}).start();
					}
					selectedModel.addElement(item);
				}
				Element buryBones = getByName(loot, "bones");
				String boneActionName = buryBones.getAttribute("value");
				BoneAction boneAction = null;
				for(BoneAction action : BoneAction.values())
					if(boneActionName.equals(action.name()))
						boneAction = action;
				switch(boneAction) {
				case NONE:
					lootBonesCheckBox.setEnabled(true);
					break;
				}
				Element radius = getByName(loot, "radius");
				lootRadiusSlider.setValue(Integer.parseInt(radius
						.getAttribute("value")));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private Element getByName(Element parent, String name) {
		return (Element) parent.getElementsByTagName(name).item(0);
	}

	private void saveConfiguration() {
		if(!saveCheckBox.isSelected())
			return;
		String directory = ".";
		File config = new File(directory, "darkfighter.xml");

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("darkfighter");
			doc.appendChild(rootElement);

			{
				Element combat = doc.createElement("combat");
				rootElement.appendChild(combat);
				Element monsters = doc.createElement("monsters");
				combat.appendChild(monsters);
				DefaultListModel<Monster> selectedModel = (DefaultListModel<Monster>) combatMonsterList
						.getModel();
				int size = selectedModel.getSize();
				for(int i = 0; i < size; i++) {
					Element element = doc.createElement("monster");
					monsters.appendChild(element);
					Monster monster = selectedModel.getElementAt(i);
					if(!(monster instanceof NearbyMonster)
							&& monster.name != null)
						element.setAttribute("name", monster.name);
					else
						element.setAttribute("id", Integer.toString(monster.id));
				}
				Element multicombat = doc.createElement("multi-combat");
				combat.appendChild(multicombat);
				multicombat.setAttribute("value",
						Boolean.toString(multiCombatCheckBox.isSelected()));
				Element radius = doc.createElement("radius");
				combat.appendChild(radius);
				radius.setAttribute("value",
						Integer.toString(combatSlider.getValue()));
			}

			{
				Element health = doc.createElement("health");
				rootElement.appendChild(health);
				Element food = doc.createElement("food");
				health.appendChild(food);
				DefaultListModel<Food> selectedModel = (DefaultListModel<Food>) selectedFoodList
						.getModel();
				int size = selectedModel.getSize();
				for(int i = 0; i < size; i++) {
					Element element = doc.createElement("item");
					food.appendChild(element);
					Food f = selectedModel.getElementAt(i);
					element.setAttribute("name", f.name());
				}
				Element eatRange = doc.createElement("eat-range");
				health.appendChild(eatRange);
				eatRange.setAttribute("low", eatLowSpinner.getValue()
						.toString());
				eatRange.setAttribute("high", eatHighSpinner.getValue()
						.toString());
				Element outOfFood = doc.createElement("out-of-food");
				health.appendChild(outOfFood);
				outOfFood.setAttribute("value",
						((NamedEmergencyAction) outOfFoodComboBox
								.getSelectedItem()).action.name());
				Element criticalHealth = doc.createElement("critical-health");
				health.appendChild(criticalHealth);
				criticalHealth.setAttribute("value",
						((NamedEmergencyAction) criticalHealthComboBox
								.getSelectedItem()).action.name());
			}

			{
				Element loot = doc.createElement("loot");
				rootElement.appendChild(loot);
				Element items = doc.createElement("items");
				loot.appendChild(items);
				DefaultListModel<Loot> selectedModel = (DefaultListModel<Loot>) lootList
						.getModel();
				int size = selectedModel.getSize();
				for(int i = 0; i < size; i++) {
					Element element = doc.createElement("item");
					items.appendChild(element);
					Loot item = selectedModel.getElementAt(i);
					if(item.name != null)
						element.setAttribute("name", item.name);
					else
						element.setAttribute("id", Integer.toString(item.id));
				}
				Element buryBones = doc.createElement("bones");
				loot.appendChild(buryBones);
				buryBones.setAttribute("value", getBoneAction().name());
				Element radius = doc.createElement("radius");
				loot.appendChild(radius);
				radius.setAttribute("value",
						Integer.toString(lootRadiusSlider.getValue()));
			}

			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(config);

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(
					"{http://xml.apache.org/xslt}indent-amount", "2");

			transformer.transform(source, result);
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Filter<NPC> getMonsterFilter() {
		return monsterFilter;
	}

	@Override
	public Filter<Item> getFoodFilter() {
		return foodFilter;
	}

	@Override
	public Filter<GroundItem> getLootFilter() {
		return lootFilter;
	}

	@Override
	public Filter<Item> getEquipFilter() {
		return null;
	}

	@Override
	public boolean shouldEat(int health, int maxHealth) {
		int foodCount = script.getContext().getInventory().getItems(foodFilter).length;
		if(lastFoodCount != foodCount) {
			lastFoodCount = foodCount;
			int low = (Integer) eatLowSpinner.getValue();
			int high = (Integer) eatHighSpinner.getValue();
			lastRandomValue = low + (int) (Math.random() * (high - low));
		}
		int percent = (int) ((health / (double) maxHealth) * 100);
		return percent < lastRandomValue;
	}

	@Override
	public EmergencyAction getOutOfFoodAction() {
		return ((NamedEmergencyAction) outOfFoodComboBox.getSelectedItem()).action;
	}

	@Override
	public EmergencyAction getCriticalHealthAction() {
		return ((NamedEmergencyAction) criticalHealthComboBox.getSelectedItem()).action;
	}

	@Override
	public BoneAction getBoneAction() {
		if(lootBonesCheckBox.isSelected())
			if(bonesBuryRadioButton.isSelected())
				return BoneAction.BURY;
			else if(bonesBananasRadioButton.isSelected())
				return BoneAction.BONES_TO_BANANAS;
			else if(bonesPeachesRadioButton.isSelected())
				return BoneAction.BONES_TO_PEACHES;
		return BoneAction.NONE;
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		new SwingConfiguration(null).awaitCompletion();
		System.exit(0);
	}

	private void updateNearbyMonsters() {
		DefaultListModel<Monster> model = (DefaultListModel<Monster>) combatMonsterList
				.getModel();
		int size = model.getSize();
		for(int i = 0; i < size; i++) {
			Monster monster = model.getElementAt(0);
			if(monster instanceof NearbyMonster)
				model.removeElement(monster);
		}

		nearbyMonsters.clear();
		if(script != null) {
			try {
				Player self = script.getContext().getPlayers().getSelf();
				if(self != null) {
					Tile location = self.getLocation();
					npcLoop: for(NPC npc : script.getContext().getNPCs()
							.getAll()) {
						for(NearbyMonster monster : nearbyMonsters)
							if(monster.id == npc.getId())
								continue npcLoop;
						nearbyMonsters.add(new NearbyMonster(npc.getName(), npc
								.getId(),
								location.distanceTo(npc.getLocation())));
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			nearbyMonsters.add(new NearbyMonster("Test", 123, 20.0));
			nearbyMonsters.add(new NearbyMonster("Lol", 324, 5.0));
			nearbyMonsters.add(new NearbyMonster("Wut", 5345, 7.1));
			nearbyMonsters.add(new NearbyMonster("OK", 12222, 24.0));
			nearbyMonsters.add(new NearbyMonster("Yeah", 99, 2.5));
		}

		updateCombat();
	}

	private void eatLowSpinnerStateChanged(ChangeEvent e) {
		SpinnerNumberModel model = (SpinnerNumberModel) eatHighSpinner
				.getModel();
		Integer value = (Integer) eatLowSpinner.getValue();
		if(value > (Integer) model.getValue())
			model.setValue(value);
		model.setMinimum(value);
		lastFoodCount = -1;
	}

	private void eatHighSpinnerStateChanged(ChangeEvent e) {
		SpinnerNumberModel model = (SpinnerNumberModel) eatLowSpinner
				.getModel();
		Integer value = (Integer) eatHighSpinner.getValue();
		if(value < (Integer) model.getValue())
			model.setValue(value);
		model.setMaximum(value);
		lastFoodCount = -1;
	}

	private void foodAddButtonActionPerformed(ActionEvent e) {
		updateFood();
		if(!foodAddButton.isEnabled())
			return;
		DefaultListModel<Food> unselectedModel = (DefaultListModel<Food>) unselectedFoodList
				.getModel();
		DefaultListModel<Food> selectedModel = (DefaultListModel<Food>) selectedFoodList
				.getModel();
		for(Food food : unselectedFoodList.getSelectedValuesList()) {
			unselectedModel.removeElement(food);
			selectedModel.addElement(food);
		}
		updateFood();
	}

	private void foodRemoveButtonActionPerformed(ActionEvent e) {
		updateFood();
		if(!foodRemoveButton.isEnabled())
			return;
		DefaultListModel<Food> unselectedModel = (DefaultListModel<Food>) unselectedFoodList
				.getModel();
		DefaultListModel<Food> selectedModel = (DefaultListModel<Food>) selectedFoodList
				.getModel();
		for(Food food : selectedFoodList.getSelectedValuesList()) {
			selectedModel.removeElement(food);
			unselectedModel.addElement(food);
		}
		updateFood();
	}

	private void foodAddAllButtonActionPerformed(ActionEvent e) {
		updateFood();
		if(!foodAddAllButton.isEnabled())
			return;
		DefaultListModel<Food> unselectedModel = (DefaultListModel<Food>) unselectedFoodList
				.getModel();
		DefaultListModel<Food> selectedModel = (DefaultListModel<Food>) selectedFoodList
				.getModel();
		int size = unselectedModel.getSize();
		for(int i = 0; i < size; i++) {
			Food food = unselectedModel.getElementAt(0);
			unselectedModel.removeElement(food);
			selectedModel.addElement(food);
		}
		updateFood();
	}

	private void foodRemoveAllButtonActionPerformed(ActionEvent e) {
		updateFood();
		if(!foodRemoveAllButton.isEnabled())
			return;
		DefaultListModel<Food> unselectedModel = (DefaultListModel<Food>) unselectedFoodList
				.getModel();
		DefaultListModel<Food> selectedModel = (DefaultListModel<Food>) selectedFoodList
				.getModel();
		int size = selectedModel.getSize();
		for(int i = 0; i < size; i++) {
			Food food = selectedModel.getElementAt(0);
			selectedModel.removeElement(food);
			unselectedModel.addElement(food);
		}
		updateFood();
	}

	private void updateFood() {
		foodAddButton.setEnabled(!unselectedFoodList.getSelectedValuesList()
				.isEmpty());
		foodAddAllButton
				.setEnabled(unselectedFoodList.getModel().getSize() > 0);

		foodRemoveButton.setEnabled(!selectedFoodList.getSelectedValuesList()
				.isEmpty());
		foodRemoveAllButton
				.setEnabled(selectedFoodList.getModel().getSize() > 0);
	}

	private void selectedFoodListValueChanged(ListSelectionEvent e) {
		updateFood();
	}

	private void unselectedFoodListValueChanged(ListSelectionEvent e) {
		updateFood();
	}

	private void combatAddButtonActionPerformed(ActionEvent e) {
		updateCombat();
		if(!combatAddButton.isEnabled())
			return;
		DefaultListModel<Monster> model = (DefaultListModel<Monster>) combatMonsterList
				.getModel();
		String type = (String) combatComboBox.getSelectedItem();
		if(type.equals("Name")) {
			model.addElement(new Monster(combatNameField.getText()));
			combatNameField.setText("");
		} else if(type.equals("ID"))
			model.addElement(new Monster((Integer) combatIdSpinner.getValue()));
		updateCombat();
	}

	private void combatMonsterListValueChanged(ListSelectionEvent e) {
		updateCombat();
	}

	private void combatNearbyAddButtonActionPerformed(ActionEvent e) {
		updateCombat();
		if(!combatNearbyAddButton.isEnabled())
			return;
		DefaultListModel<Monster> model = (DefaultListModel<Monster>) combatMonsterList
				.getModel();
		int[] selectedRows = combatNearbyMonsterList.getSelectedRows();
		List<NearbyMonster> delete = new ArrayList<>();
		for(int row : selectedRows) {
			NearbyMonster monster = nearbyMonsters.get(combatNearbyMonsterList
					.convertRowIndexToModel(row));
			delete.add(monster);
			model.addElement(monster);
		}
		nearbyMonsters.removeAll(delete);
		updateCombat();
	}

	private void combatNearbyAddAllButtonActionPerformed(ActionEvent e) {
		updateCombat();
		if(!combatNearbyAddAllButton.isEnabled())
			return;
		DefaultListModel<Monster> model = (DefaultListModel<Monster>) combatMonsterList
				.getModel();
		for(NearbyMonster monster : nearbyMonsters)
			model.addElement(monster);
		nearbyMonsters.clear();
		updateCombat();
	}

	private void combatRemoveButtonActionPerformed(ActionEvent e) {
		updateCombat();
		if(!combatRemoveButton.isEnabled())
			return;
		DefaultListModel<Monster> model = (DefaultListModel<Monster>) combatMonsterList
				.getModel();
		for(Monster monster : combatMonsterList.getSelectedValuesList()) {
			model.removeElement(monster);
			if(monster instanceof NearbyMonster)
				nearbyMonsters.add((NearbyMonster) monster);
		}
		updateCombat();
	}

	private void combatRemoveAllButtonActionPerformed(ActionEvent e) {
		updateCombat();
		if(!combatRemoveAllButton.isEnabled())
			return;
		DefaultListModel<Monster> model = (DefaultListModel<Monster>) combatMonsterList
				.getModel();
		int size = model.getSize();
		for(int i = 0; i < size; i++) {
			Monster monster = model.getElementAt(0);
			model.removeElement(monster);
			if(monster instanceof NearbyMonster)
				nearbyMonsters.add((NearbyMonster) monster);
		}
		updateCombat();
	}

	private void combatComboBoxItemStateChanged(ItemEvent e) {
		updateCombat();
	}

	private void combatNameFieldCaretUpdate(CaretEvent e) {
		updateCombat();
	}

	private void combatIdSpinnerStateChanged(ChangeEvent e) {
		updateCombat();
	}

	private void combatNearbyRefreshButtonActionPerformed(ActionEvent e) {
		updateNearbyMonsters();
	}

	private void updateCombat() {
		combatNearbyMonsterList.getSelectionModel()
				.removeListSelectionListener(nearbyMonsterListener);
		String type = (String) combatComboBox.getSelectedItem();
		((CardLayout) combatAddPanel.getLayout()).show(combatAddPanel,
				type.toLowerCase());

		DefaultListModel<Monster> model = (DefaultListModel<Monster>) combatMonsterList
				.getModel();
		boolean found = false;
		if(type.equals("Name")) {
			String text = combatNameField.getText();
			if(!text.isEmpty()) {
				for(int i = 0; i < model.size(); i++) {
					Monster monster = model.get(i);
					if(monster.name != null && text.equals(monster.name))
						found = true;
				}
			} else
				found = true;
		} else if(type.equals("ID")) {
			int id = (Integer) combatIdSpinner.getValue();
			for(int i = 0; i < model.size(); i++) {
				Monster monster = model.get(i);
				if(monster.name == null && id == monster.id)
					found = true;
			}
		}
		combatAddButton.setEnabled(!found);

		combatNearbyAddButton.setEnabled(combatNearbyMonsterList
				.getSelectedRows().length > 0);
		combatNearbyAddAllButton.setEnabled(nearbyMonsters.size() > 0);

		combatRemoveButton.setEnabled(!combatMonsterList
				.getSelectedValuesList().isEmpty());
		combatRemoveAllButton
				.setEnabled(combatMonsterList.getModel().getSize() > 0);

		int[] selectedRows = combatNearbyMonsterList.getSelectedRows();
		DefaultTableModel tableModel = (DefaultTableModel) combatNearbyMonsterList
				.getModel();
		for(int i = tableModel.getRowCount() - 1; i >= 0; i--)
			tableModel.removeRow(i);
		for(NearbyMonster monster : nearbyMonsters)
			tableModel.addRow(new Object[] { monster.name, monster.id,
					monster.distance });
		for(int i = 0; i < selectedRows.length; i++)
			if(selectedRows[i] < combatNearbyMonsterList.getRowCount())
				combatNearbyMonsterList.addRowSelectionInterval(
						selectedRows[i], selectedRows[i]);
		combatNearbyMonsterList.getSelectionModel().addListSelectionListener(
				nearbyMonsterListener);
	}

	/*private <T> void sortList(JList<T> list, Comparator<T> comparator) {
		DefaultListModel<T> model = (DefaultListModel<T>) list.getModel();

		int numItems = model.getSize();
		List<T> items = new ArrayList<>();
		for(int i = 0; i < numItems; i++)
			items.add(model.getElementAt(i));


		for(int i = 0; i < numItems; i++)
			model.setElementAt(items.get(i), i);
	}*/

	private void okButtonActionPerformed(ActionEvent e) {
		saveConfiguration();
		okPressed = true;
		setVisible(false);
	}

	private void cancelButtonActionPerformed(ActionEvent e) {
		setVisible(false);
	}

	private void lootComboBoxItemStateChanged(ItemEvent e) {
		updateLoot();
	}

	private void lootAddButtonActionPerformed(ActionEvent e) {
		updateLoot();
		if(!lootAddButton.isEnabled())
			return;
		DefaultListModel<Loot> model = (DefaultListModel<Loot>) lootList
				.getModel();
		String type = (String) lootComboBox.getSelectedItem();
		if(type.equals("Name")) {
			final Loot loot = new Loot(lootNameField.getText());
			iconRenderer.setLoading(loot, true);
			model.addElement(loot);
			lootNameField.setText("");
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						SearchResult[] results = GrandExchange.search(
								loot.name, 1);
						Icon icon = null;
						if(results.length != 0) {
							SearchResult matched = null;
							for(SearchResult result : results) {
								if(result.getName() == null)
									continue;
								if(!result.getName().toLowerCase()
										.contains(loot.name.toLowerCase()))
									continue;
								if(matched == null
										|| matched.getName().length() > result
												.getName().length())
									matched = result;
							}
							if(matched == null
									&& loot.name.toLowerCase()
											.contains("coins"))
								icon = images.getImage("COINS");
							if(matched != null) {
								URL url = new URL(
										GrandExchange
												.getLargeItemImageURL(matched
														.getId()));
								BufferedImage image = ImageIO.read(url);
								icon = new ImageIcon(image.getScaledInstance(
										16, 16, BufferedImage.SCALE_SMOOTH));
							}
						}

						final Icon lootIcon = icon;
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								iconRenderer.setLoading(loot, false);
								iconRenderer.setIcon(loot, lootIcon);
							}
						});
					} catch(Exception e) {
						e.printStackTrace();
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								iconRenderer.setLoading(loot, false);
								iconRenderer.setIcon(loot, null);
							}
						});
					}
				}
			}).start();
		} else if(type.equals("ID")) {
			final Loot loot = new Loot((Integer) lootIdSpinner.getValue());
			iconRenderer.setLoading(loot, true);
			model.addElement(loot);
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						URL url = new URL(
								GrandExchange.getLargeItemImageURL(loot.id));
						BufferedImage image = ImageIO.read(url);
						final Icon icon = new ImageIcon(
								image.getScaledInstance(16, 16,
										BufferedImage.SCALE_SMOOTH));
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								iconRenderer.setLoading(loot, false);
								iconRenderer.setIcon(loot, icon);
							}
						});
					} catch(Exception e) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								iconRenderer.setLoading(loot, false);
								iconRenderer.setIcon(loot, null);
							}
						});
					}
				}
			}).start();
		}
		updateLoot();
	}

	private void lootRemoveButtonActionPerformed(ActionEvent e) {
		updateLoot();
		if(!lootRemoveButton.isEnabled())
			return;
		DefaultListModel<Loot> model = (DefaultListModel<Loot>) lootList
				.getModel();
		List<Loot> loots = lootList.getSelectedValuesList();
		for(Loot loot : loots)
			model.removeElement(loot);
		updateLoot();
	}

	private void lootListValueChanged(ListSelectionEvent e) {
		updateLoot();
	}

	private void lootNameFieldCaretUpdate(CaretEvent e) {
		updateLoot();
	}

	private void lootIdSpinnerStateChanged(ChangeEvent e) {
		updateLoot();
	}

	private void updateLoot() {
		String type = (String) lootComboBox.getSelectedItem();
		((CardLayout) lootAddPanel.getLayout()).show(lootAddPanel,
				type.toLowerCase());

		DefaultListModel<Loot> model = (DefaultListModel<Loot>) lootList
				.getModel();
		boolean found = false;
		if(type.equals("Name")) {
			String text = lootNameField.getText();
			if(!text.isEmpty()) {
				for(int i = 0; i < model.size(); i++) {
					Loot loot = model.get(i);
					if(loot.name != null && text.equals(loot.name))
						found = true;
				}
			} else
				found = true;
		} else if(type.equals("ID")) {
			int id = (Integer) lootIdSpinner.getValue();
			for(int i = 0; i < model.size(); i++) {
				Loot loot = model.get(i);
				if(loot.name == null && loot.id == id)
					found = true;
			}
		}
		lootAddButton.setEnabled(!found);

		List<Loot> loots = lootList.getSelectedValuesList();
		lootRemoveButton.setEnabled(!loots.isEmpty());
	}

	private void lootSliderStateChanged(ChangeEvent e) {
		lootRadiusSliderLabel.setText(Integer.toString(lootRadiusSlider
				.getValue()));
	}

	private void combatSliderStateChanged(ChangeEvent e) {
		combatSliderLabel.setText(Integer.toString(combatSlider.getValue()));
	}

	private void lootEverythingCheckBoxItemStateChanged(ItemEvent e) {
		// TODO add your code here
	}

	private void equipCheckBoxItemStateChanged(ItemEvent e) {
		// TODO add your code here
	}

	private void lootDelaySliderStateChanged(ChangeEvent e) {
		// TODO add your code here
	}

	private void lootBonesCheckBoxItemStateChanged(ItemEvent e) {
		// TODO add your code here
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - DarkStorm _
		JPanel dialogPane = new JPanel();
		tabbedPane = new JTabbedPane();
		JPanel configPanel = new JPanel();
		JPanel titlePanel = new JPanel();
		titleNameLabel = new JLabel();
		titleVersionLabel = new JLabel();
		JLabel descriptionLabel = new JLabel();
		JPanel latestChangesPanel = new JPanel();
		JScrollPane latestChangesScrollPane = new JScrollPane();
		latestChangesPane = new JTextPane();
		JPanel combatPanel = new JPanel();
		JPanel combatControlsPanel = new JPanel();
		combatComboBox = new JComboBox<>();
		combatAddPanel = new JPanel();
		combatNameField = new JTextField();
		combatIdSpinner = new JSpinner();
		combatAddButton = new JButton();
		JSeparator combatSeparator1 = new JSeparator();
		JLabel combatNearbyLabel = new JLabel();
		JScrollPane combatNearbyScrollPane = new JScrollPane();
		combatNearbyMonsterList = new JTable();
		JPanel combatNearbyButtonPanel = new JPanel();
		combatNearbyAddButton = new JButton();
		combatNearbyAddAllButton = new JButton();
		combatNearbyRefreshButton = new JButton();
		JSeparator combatSeparator2 = new JSeparator();
		combatRemoveButton = new JButton();
		combatRemoveAllButton = new JButton();
		JPanel combatOptionsPanel = new JPanel();
		multiCombatCheckBox = new JCheckBox();
		JLabel combatRadiusLabel = new JLabel();
		combatSliderLabel = new JLabel();
		combatSlider = new JSlider();
		JScrollPane combatMonsterScrollPane = new JScrollPane();
		combatMonsterList = new JList<>();
		JPanel healthPanel = new JPanel();
		JScrollPane unselectedFoodListScrollPane = new JScrollPane();
		unselectedFoodList = new JList<>();
		JPanel foodButtonPanel = new JPanel();
		foodAddButton = new JButton();
		foodRemoveButton = new JButton();
		foodAddAllButton = new JButton();
		foodRemoveAllButton = new JButton();
		JScrollPane selectedFoodListScrollPane = new JScrollPane();
		selectedFoodList = new JList<>();
		JPanel healthOptionsPanel = new JPanel();
		JPanel eatBetweenPanel = new JPanel();
		JLabel eatBetweenLabel = new JLabel();
		JLabel eatLowLabel = new JLabel();
		eatLowSpinner = new JSpinner();
		JLabel eatHighLabel = new JLabel();
		eatHighSpinner = new JSpinner();
		JLabel outOfFoodLabel = new JLabel();
		outOfFoodComboBox = new JComboBox<>();
		JLabel criticalHealthLabel = new JLabel();
		criticalHealthComboBox = new JComboBox<>();
		JPanel lootPanel = new JPanel();
		lootComboBox = new JComboBox<>();
		lootAddPanel = new JPanel();
		lootIdSpinner = new JSpinner();
		lootNameField = new JTextField();
		lootAddButton = new JButton();
		lootRemoveButton = new JButton();
		JScrollPane lootScrollPane = new JScrollPane();
		lootList = new JList<>();
		panel1 = new JPanel();
		lootBonesPanel = new JPanel();
		bonesBuryRadioButton = new JRadioButton();
		bonesBananasRadioButton = new JRadioButton();
		bonesPeachesRadioButton = new JRadioButton();
		JPanel lootOptionsPanel = new JPanel();
		lootEverythingCheckBox = new JCheckBox();
		equipCheckBox = new JCheckBox();
		equipComboBox = new JComboBox<>();
		JLabel lootRadiusLabel = new JLabel();
		lootRadiusSliderLabel = new JLabel();
		lootRadiusSlider = new JSlider();
		JLabel lootDelayLabel = new JLabel();
		lootDelaySliderLabel = new JLabel();
		lootDelaySlider = new JSlider();
		JPanel buttonBar = new JPanel();
		saveCheckBox = new JCheckBox();
		okButton = new JButton();
		cancelButton = new JButton();
		lootBonesCheckBox = new JCheckBox();

		// ======== this ========
		setTitle("DarkFighter");
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// ======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(5, 5, 5, 5));

			// JFormDesigner evaluation mark
			dialogPane
					.setBorder(new javax.swing.border.CompoundBorder(
							new javax.swing.border.TitledBorder(
									new javax.swing.border.EmptyBorder(0, 0, 0,
											0), "JFormDesigner Evaluation",
									javax.swing.border.TitledBorder.CENTER,
									javax.swing.border.TitledBorder.BOTTOM,
									new java.awt.Font("Dialog",
											java.awt.Font.BOLD, 12),
									java.awt.Color.red), dialogPane.getBorder()));
			dialogPane
					.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
						@Override
						public void propertyChange(
								java.beans.PropertyChangeEvent e) {
							if("border".equals(e.getPropertyName()))
								throw new RuntimeException();
						}
					});

			dialogPane.setLayout(new BorderLayout());

			// ======== tabbedPane ========
			{

				// ======== configPanel ========
				{
					configPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
					configPanel.setLayout(new GridBagLayout());
					((GridBagLayout) configPanel.getLayout()).columnWeights = new double[] { 1.0 };
					((GridBagLayout) configPanel.getLayout()).rowWeights = new double[] {
							0.0, 0.0, 1.0 };

					// ======== titlePanel ========
					{
						titlePanel.setLayout(new GridBagLayout());

						// ---- titleNameLabel ----
						titleNameLabel.setText("DarkFighter");
						titleNameLabel
								.setHorizontalAlignment(SwingConstants.CENTER);
						titleNameLabel
								.setFont(new Font("Neuropol X Free",
										titleNameLabel.getFont().getStyle()
												| Font.BOLD, titleNameLabel
												.getFont().getSize() + 10));
						titleNameLabel.setIcon(new ImageIcon(getClass()
								.getResource("/dragon_battleaxe.png")));
						titlePanel.add(titleNameLabel, new GridBagConstraints(
								0, 0, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER,
								GridBagConstraints.BOTH,
								new Insets(0, 0, 0, 5), 0, 0));

						// ---- titleVersionLabel ----
						titleVersionLabel.setText("1.0");
						titleVersionLabel.setFont(new Font("Neuropol X Free",
								titleVersionLabel.getFont().getStyle(),
								titleVersionLabel.getFont().getSize() + 8));
						titlePanel.add(titleVersionLabel,
								new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 0, 0), 0, 0));
					}
					configPanel.add(titlePanel, new GridBagConstraints(0, 0, 1,
							1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 10, 0),
							0, 0));

					// ---- descriptionLabel ----
					descriptionLabel
							.setText("<html><body>&nbsp;&nbsp;&nbsp;&nbsp;DarkFighter - Fight anything, anywhere, any time. Use this interface to configure script options.</body></html>");
					configPanel.add(descriptionLabel, new GridBagConstraints(0,
							1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 10, 0),
							0, 0));

					// ======== latestChangesPanel ========
					{
						latestChangesPanel.setBorder(new TitledBorder(
								BorderFactory.createEmptyBorder(),
								"Latest changes"));
						latestChangesPanel.setLayout(new BorderLayout());

						// ======== latestChangesScrollPane ========
						{

							// ---- latestChangesPane ----
							latestChangesPane.setEditable(false);
							latestChangesPane.setContentType("text/html");
							latestChangesPane
									.setText("<html>\n  <body>\n    <dl>\n      <dt><strong>1.0</strong></dt>\n      <dd>Initial release</dd>\n      <dd>Fights monsters accurately</dd>\n      <dd>Loots items</dd>\n      <dd>Buries bones</dd>\n      <dd>Eats food</dd>\n      <dd>High-quality user interface</dd>\n      <dt><strong>1.1</strong></dt>\n      <dd>Tweaked combat detection, now attacks monsters much more rapidly</dd>\n      <dd>Added ability to change config after script starts</dd>\n      <dt><strong>1.11</strong></dt>\n      <dd>Temporary hotfix for a broken hook, will be releasing 1.2 with much improved combat shortly</dd>\n      <dt><strong>1.2</strong></dt>\n      <dd>Entire rewrite of practically the entire bot and script</dd>\n      <dd>Extremely accurate combat</dd>\n      <dd>Ground item collection exact</dd>\n      <dd>Ban prevention via random human-like actions</dd>\n      <dt><strong>1.21</strong></dt>\n      <dd>OSBot 1.3.1 hotfix</dd>\n      <dt><strong>1.22</strong></dt>\n      <dd>OSBot 1.3.3 hotfix</dd>\n      <dt><strong>1.3</strong></dt>\n      <dd>Eating support</dd>\n      <dd>Improved combat</dd>\n      <dd>Improved path finding</dd>\n      <dd>Improved paint info</dd>\n    </dl>\n  </body>\n</html> ");
							latestChangesPane.setAutoscrolls(false);
							latestChangesScrollPane
									.setViewportView(latestChangesPane);
						}
						latestChangesPanel.add(latestChangesScrollPane,
								BorderLayout.CENTER);
					}
					configPanel.add(latestChangesPanel, new GridBagConstraints(
							0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,
							0));
				}
				tabbedPane.addTab("DarkFighter", configPanel);

				// ======== combatPanel ========
				{
					combatPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
					combatPanel.setLayout(new GridBagLayout());
					((GridBagLayout) combatPanel.getLayout()).columnWeights = new double[] {
							0.5, 1.0 };
					((GridBagLayout) combatPanel.getLayout()).rowWeights = new double[] { 1.0 };

					// ======== combatControlsPanel ========
					{
						combatControlsPanel.setLayout(new GridBagLayout());
						((GridBagLayout) combatControlsPanel.getLayout()).columnWeights = new double[] {
								0.0, 1.0, 0.0 };
						((GridBagLayout) combatControlsPanel.getLayout()).rowWeights = new double[] {
								0.0, 0.0, 1.0, 0.0 };

						// ---- combatComboBox ----
						combatComboBox.setModel(new DefaultComboBoxModel<>(
								new String[] { "Name", "ID" }));
						combatComboBox.addItemListener(new ItemListener() {
							@Override
							public void itemStateChanged(ItemEvent e) {
								combatComboBoxItemStateChanged(e);
							}
						});
						combatControlsPanel.add(combatComboBox,
								new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 5, 5), 0, 0));

						// ======== combatAddPanel ========
						{
							combatAddPanel.setLayout(new CardLayout());

							// ---- combatNameField ----
							combatNameField
									.addCaretListener(new CaretListener() {
										@Override
										public void caretUpdate(CaretEvent e) {
											combatNameFieldCaretUpdate(e);
										}
									});
							combatAddPanel.add(combatNameField, "name");

							// ---- combatIdSpinner ----
							combatIdSpinner
									.addChangeListener(new ChangeListener() {
										@Override
										public void stateChanged(ChangeEvent e) {
											combatIdSpinnerStateChanged(e);
										}
									});
							combatAddPanel.add(combatIdSpinner, "id");
						}
						combatControlsPanel.add(combatAddPanel,
								new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 5, 5), 0, 0));

						// ---- combatAddButton ----
						combatAddButton.setIcon(new ImageIcon(getClass()
								.getResource("/add.png")));
						combatAddButton.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								combatAddButtonActionPerformed(e);
							}
						});
						combatControlsPanel.add(combatAddButton,
								new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 5, 0), 0, 0));
						combatControlsPanel.add(combatSeparator1,
								new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 5, 0), 0, 0));

						// ---- combatNearbyLabel ----
						combatNearbyLabel.setText("Nearby:");
						combatNearbyLabel
								.setVerticalAlignment(SwingConstants.TOP);
						combatControlsPanel.add(combatNearbyLabel,
								new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 5, 5), 0, 0));

						// ======== combatNearbyScrollPane ========
						{

							// ---- combatNearbyMonsterList ----
							combatNearbyMonsterList
									.setModel(new DefaultTableModel(
											new Object[][] { { null, null, null }, },
											new String[] { "Name", "ID",
													"Distance" }) {
										Class<?>[] columnTypes = new Class<?>[] {
												String.class, Integer.class,
												Double.class };
										boolean[] columnEditable = new boolean[] {
												false, false, false };

										@Override
										public Class<?> getColumnClass(
												int columnIndex) {
											return columnTypes[columnIndex];
										}

										@Override
										public boolean isCellEditable(
												int rowIndex, int columnIndex) {
											return columnEditable[columnIndex];
										}
									});
							combatNearbyMonsterList
									.setAutoCreateRowSorter(true);
							combatNearbyScrollPane
									.setViewportView(combatNearbyMonsterList);
						}
						combatControlsPanel.add(combatNearbyScrollPane,
								new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 5, 5), 0, 0));

						// ======== combatNearbyButtonPanel ========
						{
							combatNearbyButtonPanel
									.setLayout(new GridBagLayout());
							((GridBagLayout) combatNearbyButtonPanel
									.getLayout()).columnWidths = new int[] { 0,
									0 };
							((GridBagLayout) combatNearbyButtonPanel
									.getLayout()).rowHeights = new int[] { 0,
									0, 0, 0, 0, 0, 0, 0 };
							((GridBagLayout) combatNearbyButtonPanel
									.getLayout()).columnWeights = new double[] {
									0.0, 1.0E-4 };
							((GridBagLayout) combatNearbyButtonPanel
									.getLayout()).rowWeights = new double[] {
									0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0E-4 };

							// ---- combatNearbyAddButton ----
							combatNearbyAddButton.setIcon(new ImageIcon(
									getClass().getResource("/forward.png")));
							combatNearbyAddButton
									.addActionListener(new ActionListener() {
										@Override
										public void actionPerformed(
												ActionEvent e) {
											combatNearbyAddButtonActionPerformed(e);
										}
									});
							combatNearbyButtonPanel.add(combatNearbyAddButton,
									new GridBagConstraints(0, 0, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- combatNearbyAddAllButton ----
							combatNearbyAddAllButton.setIcon(new ImageIcon(
									getClass().getResource("/next.png")));
							combatNearbyAddAllButton
									.addActionListener(new ActionListener() {
										@Override
										public void actionPerformed(
												ActionEvent e) {
											combatNearbyAddAllButtonActionPerformed(e);
										}
									});
							combatNearbyButtonPanel.add(
									combatNearbyAddAllButton,
									new GridBagConstraints(0, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- combatNearbyRefreshButton ----
							combatNearbyRefreshButton.setIcon(new ImageIcon(
									getClass().getResource("/refresh.png")));
							combatNearbyRefreshButton
									.addActionListener(new ActionListener() {
										@Override
										public void actionPerformed(
												ActionEvent e) {
											combatNearbyRefreshButtonActionPerformed(e);
										}
									});
							combatNearbyButtonPanel.add(
									combatNearbyRefreshButton,
									new GridBagConstraints(0, 2, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));
							combatNearbyButtonPanel.add(combatSeparator2,
									new GridBagConstraints(0, 4, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- combatRemoveButton ----
							combatRemoveButton.setIcon(new ImageIcon(getClass()
									.getResource("/remove.png")));
							combatRemoveButton
									.addActionListener(new ActionListener() {
										@Override
										public void actionPerformed(
												ActionEvent e) {
											combatRemoveButtonActionPerformed(e);
										}
									});
							combatNearbyButtonPanel.add(combatRemoveButton,
									new GridBagConstraints(0, 5, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- combatRemoveAllButton ----
							combatRemoveAllButton.setIcon(new ImageIcon(
									getClass().getResource("/cancel.png")));
							combatRemoveAllButton
									.addActionListener(new ActionListener() {
										@Override
										public void actionPerformed(
												ActionEvent e) {
											combatRemoveAllButtonActionPerformed(e);
										}
									});
							combatNearbyButtonPanel.add(combatRemoveAllButton,
									new GridBagConstraints(0, 6, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 0), 0, 0));
						}
						combatControlsPanel.add(combatNearbyButtonPanel,
								new GridBagConstraints(2, 2, 1, 2, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 0, 0), 0, 0));

						// ======== combatOptionsPanel ========
						{
							combatOptionsPanel.setBorder(new TitledBorder(
									"Options"));
							combatOptionsPanel.setLayout(new GridBagLayout());
							((GridBagLayout) combatOptionsPanel.getLayout()).columnWidths = new int[] {
									0, 0, 0, 0 };
							((GridBagLayout) combatOptionsPanel.getLayout()).rowHeights = new int[] {
									0, 0, 0 };
							((GridBagLayout) combatOptionsPanel.getLayout()).columnWeights = new double[] {
									0.0, 0.0, 1.0, 1.0E-4 };
							((GridBagLayout) combatOptionsPanel.getLayout()).rowWeights = new double[] {
									0.0, 0.0, 1.0E-4 };

							// ---- multiCombatCheckBox ----
							multiCombatCheckBox
									.setText("Attack in-combat monsters");
							combatOptionsPanel.add(multiCombatCheckBox,
									new GridBagConstraints(0, 0, 3, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- combatRadiusLabel ----
							combatRadiusLabel.setText("Combat radius:");
							combatOptionsPanel.add(combatRadiusLabel,
									new GridBagConstraints(0, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 5), 0, 0));

							// ---- combatSliderLabel ----
							combatSliderLabel.setText("50");
							combatOptionsPanel.add(combatSliderLabel,
									new GridBagConstraints(1, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 5), 0, 0));

							// ---- combatSlider ----
							combatSlider.setMinorTickSpacing(5);
							combatSlider.setMajorTickSpacing(25);
							combatSlider.setValue(16);
							combatSlider.setSnapToTicks(true);
							combatSlider
									.addChangeListener(new ChangeListener() {
										@Override
										public void stateChanged(ChangeEvent e) {
											combatSliderStateChanged(e);
										}
									});
							combatOptionsPanel.add(combatSlider,
									new GridBagConstraints(2, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 0), 0, 0));
						}
						combatControlsPanel.add(combatOptionsPanel,
								new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 0, 5), 0, 0));
					}
					combatPanel.add(combatControlsPanel,
							new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER,
									GridBagConstraints.BOTH, new Insets(0, 0,
											0, 5), 0, 0));

					// ======== combatMonsterScrollPane ========
					{

						// ---- combatMonsterList ----
						combatMonsterList
								.setModel(new DefaultListModel<Monster>());
						combatMonsterList
								.addListSelectionListener(new ListSelectionListener() {
									@Override
									public void valueChanged(
											ListSelectionEvent e) {
										combatMonsterListValueChanged(e);
									}
								});
						combatMonsterScrollPane
								.setViewportView(combatMonsterList);
					}
					combatPanel.add(combatMonsterScrollPane,
							new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER,
									GridBagConstraints.BOTH, new Insets(0, 0,
											0, 0), 0, 0));
				}
				tabbedPane.addTab("Combat", combatPanel);

				// ======== healthPanel ========
				{
					healthPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
					healthPanel.setLayout(new GridBagLayout());
					((GridBagLayout) healthPanel.getLayout()).columnWeights = new double[] {
							1.0, 0.0, 1.0 };
					((GridBagLayout) healthPanel.getLayout()).rowWeights = new double[] {
							1.0, 0.0 };

					// ======== unselectedFoodListScrollPane ========
					{

						// ---- unselectedFoodList ----
						unselectedFoodList.setCellRenderer(iconRenderer);
						unselectedFoodList
								.setModel(new DefaultListModel<Food>());
						unselectedFoodList
								.addListSelectionListener(new ListSelectionListener() {
									@Override
									public void valueChanged(
											ListSelectionEvent e) {
										unselectedFoodListValueChanged(e);
									}
								});
						unselectedFoodListScrollPane
								.setViewportView(unselectedFoodList);
					}
					healthPanel.add(unselectedFoodListScrollPane,
							new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER,
									GridBagConstraints.BOTH, new Insets(0, 0,
											5, 5), 0, 0));

					// ======== foodButtonPanel ========
					{
						foodButtonPanel.setLayout(new GridLayout(0, 1, 5, 5));

						// ---- foodAddButton ----
						foodAddButton.setIcon(new ImageIcon(getClass()
								.getResource("/forward.png")));
						foodAddButton.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								foodAddButtonActionPerformed(e);
							}
						});
						foodButtonPanel.add(foodAddButton);

						// ---- foodRemoveButton ----
						foodRemoveButton.setIcon(new ImageIcon(getClass()
								.getResource("/backward.png")));
						foodRemoveButton
								.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										foodRemoveButtonActionPerformed(e);
									}
								});
						foodButtonPanel.add(foodRemoveButton);

						// ---- foodAddAllButton ----
						foodAddAllButton.setIcon(new ImageIcon(getClass()
								.getResource("/next.png")));
						foodAddAllButton
								.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										foodAddAllButtonActionPerformed(e);
									}
								});
						foodButtonPanel.add(foodAddAllButton);

						// ---- foodRemoveAllButton ----
						foodRemoveAllButton.setIcon(new ImageIcon(getClass()
								.getResource("/previous.png")));
						foodRemoveAllButton
								.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										foodRemoveAllButtonActionPerformed(e);
									}
								});
						foodButtonPanel.add(foodRemoveAllButton);
					}
					healthPanel.add(foodButtonPanel, new GridBagConstraints(1,
							0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0,
							0));

					// ======== selectedFoodListScrollPane ========
					{

						// ---- selectedFoodList ----
						selectedFoodList.setCellRenderer(iconRenderer);
						selectedFoodList.setModel(new DefaultListModel<Food>());
						selectedFoodList
								.addListSelectionListener(new ListSelectionListener() {
									@Override
									public void valueChanged(
											ListSelectionEvent e) {
										selectedFoodListValueChanged(e);
									}
								});
						selectedFoodListScrollPane
								.setViewportView(selectedFoodList);
					}
					healthPanel.add(selectedFoodListScrollPane,
							new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER,
									GridBagConstraints.BOTH, new Insets(0, 0,
											5, 0), 0, 0));

					// ======== healthOptionsPanel ========
					{
						healthOptionsPanel.setLayout(new GridBagLayout());
						((GridBagLayout) healthOptionsPanel.getLayout()).columnWidths = new int[] {
								0, 0 };
						((GridBagLayout) healthOptionsPanel.getLayout()).rowHeights = new int[] {
								0, 0 };
						((GridBagLayout) healthOptionsPanel.getLayout()).columnWeights = new double[] {
								1.0, 1.0E-4 };
						((GridBagLayout) healthOptionsPanel.getLayout()).rowWeights = new double[] {
								0.0, 1.0E-4 };

						// ======== eatBetweenPanel ========
						{
							eatBetweenPanel.setBorder(new TitledBorder(
									"Options"));
							eatBetweenPanel.setLayout(new GridBagLayout());
							((GridBagLayout) eatBetweenPanel.getLayout()).columnWidths = new int[] {
									0, 0, 0, 0, 0, 0 };
							((GridBagLayout) eatBetweenPanel.getLayout()).rowHeights = new int[] {
									0, 0, 0, 0 };
							((GridBagLayout) eatBetweenPanel.getLayout()).columnWeights = new double[] {
									1.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
							((GridBagLayout) eatBetweenPanel.getLayout()).rowWeights = new double[] {
									0.0, 0.0, 0.0, 1.0E-4 };

							// ---- eatBetweenLabel ----
							eatBetweenLabel.setText("Eat when below (%):");
							eatBetweenPanel.add(eatBetweenLabel,
									new GridBagConstraints(0, 0, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));

							// ---- eatLowLabel ----
							eatLowLabel.setText("Low");
							eatBetweenPanel.add(eatLowLabel,
									new GridBagConstraints(1, 0, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));

							// ---- eatLowSpinner ----
							eatLowSpinner.setModel(new SpinnerNumberModel(40,
									0, 60, 1));
							eatLowSpinner
									.addChangeListener(new ChangeListener() {
										@Override
										public void stateChanged(ChangeEvent e) {
											eatLowSpinnerStateChanged(e);
										}
									});
							eatBetweenPanel.add(eatLowSpinner,
									new GridBagConstraints(2, 0, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));

							// ---- eatHighLabel ----
							eatHighLabel.setText("High");
							eatBetweenPanel.add(eatHighLabel,
									new GridBagConstraints(3, 0, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));

							// ---- eatHighSpinner ----
							eatHighSpinner.setModel(new SpinnerNumberModel(60,
									40, 100, 1));
							eatHighSpinner
									.addChangeListener(new ChangeListener() {
										@Override
										public void stateChanged(ChangeEvent e) {
											eatHighSpinnerStateChanged(e);
										}
									});
							eatBetweenPanel.add(eatHighSpinner,
									new GridBagConstraints(4, 0, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- outOfFoodLabel ----
							outOfFoodLabel.setText("When out of food:");
							eatBetweenPanel.add(outOfFoodLabel,
									new GridBagConstraints(0, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));

							// ---- outOfFoodComboBox ----
							outOfFoodComboBox
									.setModel(new DefaultComboBoxModel<NamedEmergencyAction>());
							eatBetweenPanel.add(outOfFoodComboBox,
									new GridBagConstraints(1, 1, 4, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- criticalHealthLabel ----
							criticalHealthLabel
									.setText("When critical health (and no food):");
							eatBetweenPanel.add(criticalHealthLabel,
									new GridBagConstraints(0, 2, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 5), 0, 0));

							// ---- criticalHealthComboBox ----
							criticalHealthComboBox
									.setModel(new DefaultComboBoxModel<NamedEmergencyAction>());
							eatBetweenPanel.add(criticalHealthComboBox,
									new GridBagConstraints(1, 2, 4, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 0), 0, 0));
						}
						healthOptionsPanel.add(eatBetweenPanel,
								new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
										GridBagConstraints.CENTER,
										GridBagConstraints.BOTH, new Insets(0,
												0, 0, 0), 0, 0));
					}
					healthPanel.add(healthOptionsPanel, new GridBagConstraints(
							0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,
							0));
				}
				tabbedPane.addTab("Health", healthPanel);

				// ======== lootPanel ========
				{
					lootPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
					lootPanel.setLayout(new GridBagLayout());
					((GridBagLayout) lootPanel.getLayout()).columnWidths = new int[] {
							0, 0, 0, 0, 0, 0 };
					((GridBagLayout) lootPanel.getLayout()).rowHeights = new int[] {
							0, 0, 0 };
					((GridBagLayout) lootPanel.getLayout()).columnWeights = new double[] {
							0.0, 1.0, 0.0, 0.0, 0.0, 1.0E-4 };
					((GridBagLayout) lootPanel.getLayout()).rowWeights = new double[] {
							0.0, 1.0, 1.0E-4 };

					// ---- lootComboBox ----
					lootComboBox.setModel(new DefaultComboBoxModel<>(
							new String[] { "ID" }));
					lootComboBox.addItemListener(new ItemListener() {
						@Override
						public void itemStateChanged(ItemEvent e) {
							lootComboBoxItemStateChanged(e);
						}
					});
					lootPanel.add(lootComboBox, new GridBagConstraints(0, 0, 1,
							1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0,
							0));

					// ======== lootAddPanel ========
					{
						lootAddPanel.setLayout(new CardLayout());

						// ---- lootIdSpinner ----
						lootIdSpinner.setModel(new SpinnerNumberModel(1, 1,
								null, 1));
						lootIdSpinner.addChangeListener(new ChangeListener() {
							@Override
							public void stateChanged(ChangeEvent e) {
								lootIdSpinnerStateChanged(e);
							}
						});
						lootAddPanel.add(lootIdSpinner, "id");

						// ---- lootNameField ----
						lootNameField.addCaretListener(new CaretListener() {
							@Override
							public void caretUpdate(CaretEvent e) {
								lootNameFieldCaretUpdate(e);
							}
						});
						lootAddPanel.add(lootNameField, "name");
					}
					lootPanel.add(lootAddPanel, new GridBagConstraints(1, 0, 1,
							1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0,
							0));

					// ---- lootAddButton ----
					lootAddButton.setIcon(new ImageIcon(getClass().getResource(
							"/add.png")));
					lootAddButton.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							lootAddButtonActionPerformed(e);
						}
					});
					lootPanel.add(lootAddButton, new GridBagConstraints(2, 0,
							1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0,
							0));

					// ---- lootRemoveButton ----
					lootRemoveButton.setIcon(new ImageIcon(getClass()
							.getResource("/remove.png")));
					lootRemoveButton.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							lootRemoveButtonActionPerformed(e);
						}
					});
					lootPanel.add(lootRemoveButton, new GridBagConstraints(3,
							0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0,
							0));

					// ======== lootScrollPane ========
					{

						// ---- lootList ----
						lootList.setCellRenderer(iconRenderer);
						lootList.setModel(new DefaultListModel<Loot>());
						lootList.addListSelectionListener(new ListSelectionListener() {
							@Override
							public void valueChanged(ListSelectionEvent e) {
								lootListValueChanged(e);
							}
						});
						lootScrollPane.setViewportView(lootList);
					}
					lootPanel.add(lootScrollPane, new GridBagConstraints(0, 1,
							4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0,
							0));

					// ======== panel1 ========
					{
						panel1.setLayout(new GridBagLayout());
						((GridBagLayout) panel1.getLayout()).columnWidths = new int[] {
								0, 0 };
						((GridBagLayout) panel1.getLayout()).rowHeights = new int[] {
								0, 0, 0 };
						((GridBagLayout) panel1.getLayout()).columnWeights = new double[] {
								1.0, 1.0E-4 };
						((GridBagLayout) panel1.getLayout()).rowWeights = new double[] {
								0.0, 1.0, 1.0E-4 };

						// ======== lootBonesPanel ========
						{
							lootBonesPanel.setBorder(new TitledBorder("Bones"));
							lootBonesPanel.setLayout(new GridBagLayout());
							((GridBagLayout) lootBonesPanel.getLayout()).columnWidths = new int[] {
									0, 0 };
							((GridBagLayout) lootBonesPanel.getLayout()).rowHeights = new int[] {
									0, 0, 0, 0 };
							((GridBagLayout) lootBonesPanel.getLayout()).columnWeights = new double[] {
									1.0, 1.0E-4 };
							((GridBagLayout) lootBonesPanel.getLayout()).rowWeights = new double[] {
									0.0, 0.0, 0.0, 1.0E-4 };

							// ---- bonesBuryRadioButton ----
							bonesBuryRadioButton.setText("Bury");
							lootBonesPanel.add(bonesBuryRadioButton,
									new GridBagConstraints(0, 0, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- bonesBananasRadioButton ----
							bonesBananasRadioButton.setText("To Bananas");
							lootBonesPanel.add(bonesBananasRadioButton,
									new GridBagConstraints(0, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- bonesPeachesRadioButton ----
							bonesPeachesRadioButton.setText("To Peaches");
							lootBonesPanel.add(bonesPeachesRadioButton,
									new GridBagConstraints(0, 2, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 0), 0, 0));
						}
						panel1.add(lootBonesPanel, new GridBagConstraints(0, 0,
								1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
								GridBagConstraints.BOTH,
								new Insets(0, 0, 5, 0), 0, 0));

						// ======== lootOptionsPanel ========
						{
							lootOptionsPanel.setBorder(new TitledBorder(
									"Options"));
							lootOptionsPanel.setLayout(new GridBagLayout());
							((GridBagLayout) lootOptionsPanel.getLayout()).columnWidths = new int[] {
									0, 0, 0 };
							((GridBagLayout) lootOptionsPanel.getLayout()).rowHeights = new int[] {
									0, 0, 0, 0, 0, 0, 0 };
							((GridBagLayout) lootOptionsPanel.getLayout()).columnWeights = new double[] {
									0.0, 0.0, 1.0E-4 };
							((GridBagLayout) lootOptionsPanel.getLayout()).rowWeights = new double[] {
									0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };

							// ---- lootEverythingCheckBox ----
							lootEverythingCheckBox.setText("Loot everything");
							lootEverythingCheckBox
									.addItemListener(new ItemListener() {
										@Override
										public void itemStateChanged(ItemEvent e) {
											lootEverythingCheckBoxItemStateChanged(e);
										}
									});
							lootOptionsPanel.add(lootEverythingCheckBox,
									new GridBagConstraints(0, 0, 2, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- equipCheckBox ----
							equipCheckBox.setText("Equip");
							equipCheckBox.addItemListener(new ItemListener() {
								@Override
								public void itemStateChanged(ItemEvent e) {
									equipCheckBoxItemStateChanged(e);
								}
							});
							lootOptionsPanel.add(equipCheckBox,
									new GridBagConstraints(0, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));
							lootOptionsPanel.add(equipComboBox,
									new GridBagConstraints(1, 1, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- lootRadiusLabel ----
							lootRadiusLabel.setText("Loot radius:");
							lootOptionsPanel.add(lootRadiusLabel,
									new GridBagConstraints(0, 2, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));

							// ---- lootRadiusSliderLabel ----
							lootRadiusSliderLabel.setText("50");
							lootRadiusSliderLabel
									.setHorizontalAlignment(SwingConstants.TRAILING);
							lootOptionsPanel.add(lootRadiusSliderLabel,
									new GridBagConstraints(1, 2, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- lootRadiusSlider ----
							lootRadiusSlider.setMajorTickSpacing(25);
							lootRadiusSlider.setMinorTickSpacing(5);
							lootRadiusSlider.setValue(8);
							lootRadiusSlider.setSnapToTicks(true);
							lootRadiusSlider
									.addChangeListener(new ChangeListener() {
										@Override
										public void stateChanged(ChangeEvent e) {
											lootSliderStateChanged(e);
										}
									});
							lootOptionsPanel.add(lootRadiusSlider,
									new GridBagConstraints(0, 3, 2, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- lootDelayLabel ----
							lootDelayLabel.setText("Loot delay:");
							lootOptionsPanel.add(lootDelayLabel,
									new GridBagConstraints(0, 4, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 5), 0, 0));

							// ---- lootDelaySliderLabel ----
							lootDelaySliderLabel.setText("50 seconds");
							lootDelaySliderLabel
									.setHorizontalAlignment(SwingConstants.TRAILING);
							lootOptionsPanel.add(lootDelaySliderLabel,
									new GridBagConstraints(1, 4, 1, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 5, 0), 0, 0));

							// ---- lootDelaySlider ----
							lootDelaySlider.setValue(0);
							lootDelaySlider.setSnapToTicks(true);
							lootDelaySlider
									.addChangeListener(new ChangeListener() {
										@Override
										public void stateChanged(ChangeEvent e) {
											lootDelaySliderStateChanged(e);
										}
									});
							lootOptionsPanel.add(lootDelaySlider,
									new GridBagConstraints(0, 5, 2, 1, 0.0,
											0.0, GridBagConstraints.CENTER,
											GridBagConstraints.BOTH,
											new Insets(0, 0, 0, 0), 0, 0));
						}
						panel1.add(lootOptionsPanel, new GridBagConstraints(0,
								1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
								GridBagConstraints.BOTH,
								new Insets(0, 0, 0, 0), 0, 0));
					}
					lootPanel.add(panel1, new GridBagConstraints(4, 0, 1, 2,
							0.0, 0.0, GridBagConstraints.CENTER,
							GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,
							0));
				}
				tabbedPane.addTab("Loot", lootPanel);

			}
			dialogPane.add(tabbedPane, BorderLayout.CENTER);

			// ======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] {
						0, 0, 85, 80 };
				((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] {
						1.0, 0.0, 0.0, 0.0 };

				// ---- saveCheckBox ----
				saveCheckBox.setText("Save");
				saveCheckBox.setSelected(true);
				buttonBar.add(saveCheckBox, new GridBagConstraints(1, 0, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

				// ---- okButton ----
				okButton.setText("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						okButtonActionPerformed(e);
					}
				});
				buttonBar.add(okButton, new GridBagConstraints(2, 0, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

				// ---- cancelButton ----
				cancelButton.setText("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						cancelButtonActionPerformed(e);
					}
				});
				buttonBar.add(cancelButton, new GridBagConstraints(3, 0, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		setSize(530, 355);
		setLocationRelativeTo(getOwner());

		// ---- lootBonesCheckBox ----
		lootBonesCheckBox.setText("Bones");
		lootBonesCheckBox.setSelected(true);
		lootBonesCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				lootBonesCheckBoxItemStateChanged(e);
			}
		});

		// ---- buttonGroup1 ----
		ButtonGroup buttonGroup1 = new ButtonGroup();
		buttonGroup1.add(bonesBuryRadioButton);
		buttonGroup1.add(bonesBananasRadioButton);
		buttonGroup1.add(bonesPeachesRadioButton);
		// //GEN-END:initComponents
	}

	private class Monster {
		protected final int id;
		protected final String name;

		public Monster(int id) {
			this.id = id;
			name = null;
		}

		public Monster(String name) {
			this.name = name;
			id = 0;
		}

		protected Monster(String name, int id) {
			this.name = name;
			this.id = id;
		}

		@Override
		public String toString() {
			if(name != null)
				return name;
			return Integer.toString(id);
		}
	}

	private class NearbyMonster extends Monster {
		private final double distance;

		public NearbyMonster(String name, int id, double distance) {
			super(name, id);
			this.distance = distance;
		}

		@Override
		public String toString() {
			return name + " (" + id + ")";
		}
	}

	private class Loot {
		private final int id;
		private final String name;

		public Loot(int id) {
			this.id = id;
			name = null;
		}

		public Loot(String name) {
			this.name = name;
			id = 0;
		}

		@Override
		public String toString() {
			if(name != null)
				return name;
			return Integer.toString(id);
		}
	}

	private enum Food {
		SHRIMPS("Shrimps", 315),
		ANCHOVIES("Anchovies", 319),
		SARDINE("Sardine", 325),
		SALMON("Salmon", 329),
		TROUT("Trout", 333),
		COD("Cod", 339),
		RAW_HERRING("Raw herring", 345),
		HERRING("Herring", 347),
		PIKE("Pike", 351),
		MACKEREL("Mackerel", 355),
		TUNA("Tuna", 361),
		BASS("Bass", 365),
		SWORDFISH("Swordfish", 373),
		LOBSTER("Lobster", 379),
		SHARK("Shark", 385),
		MANTA_RAY("Manta ray", 391),
		SEA_TURTLE("Sea turtle", 397),
		PITTA_BREAD("Pitta bread", 1865),
		CAKE("Cake", 1891),
		TWO_THIRDS_CAKE("2/3 cake", 1893),
		SLICE_OF_CAKE("Slice of cake", 1895),
		CHOCOLATE_CAKE("Chocolate cake", 1897),
		TWO_THIRDS_CHOCOLATE_CAKE("2/3 chocolate cake", 1899),
		PLAIN_PIZZA("Plain pizza", 2289),
		ONE_HALF_PLAIN_PIZZA("1/2 plain pizza", 2291),
		MEAT_PIZZA("Meat pizza", 2293),
		ONE_HALF_MEAT_PIZZA("1/2 meat pizza", 2295),
		ANCHOVY_PIZZA("Anchovy pizza", 2297),
		ONE_HALF_ANCHOVY_PIZZA("1/2 anchovy pizza", 2299),
		PINEAPPLE_PIZZA("Pineapple pizza", 2301),
		ONE_HALF_PINEAPPLE_PIZZA("1/2 p'apple pizza", 2303),
		BREAD_DOUGH("Bread dough", 2307),
		BREAD("Bread", 2309),
		APPLE_PIE("Apple pie", 2323),
		REDBERRY_PIE("Redberry pie", 2325),
		MEAT_PIE("Meat pie", 2327),
		HALF_A_MEAT_PIE("Half a meat pie", 2331),
		HALF_A_REDBERRY_PIE("Half a redberry pie", 2333),
		HALF_AN_APPLE_PIE("Half an apple pie", 2335),
		COOKED_OOMLIE_WRAP("Cooked oomlie wrap", 2343),
		COOKED_KARAMBWAN("Cooked karambwan", 3144),
		COOKED_RABBIT("Cooked rabbit", 3228),
		COOKED_SLIMY_EEL("Cooked slimy eel", 3381),
		COOKED_CHICKEN("Cooked chicken", 2140, 4291),
		COOKED_MEAT("Cooked meat", 2142, 4293),
		POTATO_WITH_BUTTER("Potato with butter", 6703),
		POTATO_WITH_CHEESE("Potato with cheese", 6705),
		TUNA_POTATO("Tuna potato", 7060),
		TUNA_AND_CORN("Tuna and corn", 7068),
		CHOPPED_TUNA("Chopped tuna", 7086, 7164, 7166),
		RAW_MUD_PIE("Raw mud pie", 7168),
		MUD_PIE("Mud pie", 7170),
		PART_GARDEN_PIE("Part garden pie", 7172, 7174),
		RAW_GARDEN_PIE("Raw garden pie", 7176),
		GARDEN_PIE("Garden pie", 7178),
		HALF_A_GARDEN_PIE("Half a garden pie", 7180),
		PART_FISH_PIE("Part fish pie", 7182, 7184),
		FISH_PIE("Fish pie", 7188),
		HALF_A_FISH_PIE("Half a fish pie", 7190),
		PART_ADMIRAL_PIE("Part admiral pie", 7192, 7194),
		RAW_ADMIRAL_PIE("Raw admiral pie", 7196),
		ADMIRAL_PIE("Admiral pie", 7198),
		HALF_AN_ADMIRAL_PIE("Half an admiral pie", 7200),
		PART_WILD_PIE("Part wild pie", 7202, 7204),
		WILD_PIE("Wild pie", 7208),
		HALF_A_WILD_PIE("Half a wild pie", 7210, 7212, 7214),
		RAW_SUMMER_PIE("Raw summer pie", 7216),
		SUMMER_PIE("Summer pie", 7218),
		HALF_A_SUMMER_PIE("Half a summer pie", 7220),
		COOKED_CHOMPY("Cooked chompy", 2878, 7228),
		COOKED_CRAB_MEAT("Cooked crab meat", 7521),
		COOKED_JUBBLY("Cooked jubbly", 7568),
		MONKFISH("Monkfish", 7946),
		MINT_CAKE("Mint cake", 9475),
		LEAPING_TROUT("Leaping trout", 11328),
		LEAPING_SALMON("Leaping salmon", 11330);

		private final String name;
		private final int[] ids;
		private final Icon icon;

		private Food(String name, int... ids) {
			this.name = name;
			this.ids = ids;
			icon = images.getImage(name());
		}

		public String getName() {
			return name;
		}

		public int[] getIds() {
			return ids.clone();
		}

		public Icon getIcon() {
			return icon;
		}

		@Override
		public String toString() {
			StringBuffer buffer = new StringBuffer(getName());
			int[] ids = getIds();
			if(ids.length > 0) {
				buffer.append(" (").append(ids[0]);
				for(int i = 1; i < ids.length; i++)
					buffer.append(", ").append(ids[i]);
				buffer.append(")");
			}
			return buffer.toString();
		}
	}

	@SuppressWarnings("unused")
	private enum Equippable {
		BRONZE_ARROW("Bronze Arrow", 882, 883, 5616, 5622),
		IRON_ARROW("Iron Arrow", 884, 885, 5617, 5623),
		STEEL_ARROW("Steel Arrow", 886, 887, 5618, 5624),
		MITHRIL_ARROW("Mithril Arrow", 888, 889, 5619, 5625),
		ADAMANT_ARROW("Adamant Arrow", 890, 891, 5620, 5626),
		RUNE_ARROW("Rune Arrow", 892, 893, 5621, 5627),
		OGRE_ARROW("Ogre Arrow", 2866),
		BRONZE_BOLT("Bronze Bolt", 1),
		BLURITE_BOLT("", 1),
		IRON_BOLT("", 123),
		STEEL_BOLT("", 123),
		MITHRIL_BOLT("", 123),
		ADAMANT_BOLT("", 123),
		RUNE_BOLT("", 123),

		;

		private final String name;
		private final int[] ids;
		private final Icon icon;

		private Equippable(String name, int... ids) {
			this.name = name;
			this.ids = ids;
			icon = images.getImage(name());
		}

		public String getName() {
			return name;
		}

		public int[] getIds() {
			return ids.clone();
		}

		public Icon getIcon() {
			return icon;
		}

		@Override
		public String toString() {
			StringBuffer buffer = new StringBuffer(getName());
			int[] ids = getIds();
			if(ids.length > 0) {
				buffer.append(" (").append(ids[0]);
				for(int i = 1; i < ids.length; i++)
					buffer.append(", ").append(ids[i]);
				buffer.append(")");
			}
			return buffer.toString();
		}
	}

	public static class IconListRenderer extends DefaultListCellRenderer {
		private final Map<Object, Icon> icons;
		private final List<Object> loading;
		private final List<JList<?>> lists;
		private final ImageIcon[] loadingFrames;

		private int loadingIndex = 0;

		public IconListRenderer() {
			icons = new HashMap<>();
			loading = new ArrayList<>();
			lists = new ArrayList<>();

			{
				ImageIcon[] loadingFrames = new ImageIcon[11];
				try {
					for(int i = 0; i < loadingFrames.length; i++)
						loadingFrames[i] = new ImageIcon(
								ImageIO.read(getClass().getResource(
										"/loading-" + ((i + 1) < 10 ? "0" : "")
												+ (i + 1) + ".png")));
				} catch(IOException exception) {
					exception.printStackTrace();
				}
				this.loadingFrames = loadingFrames;
			}

			final Timer timer = new Timer(100, null);
			ActionListener actionListener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if(++loadingIndex >= loadingFrames.length)
						loadingIndex = 0;

					for(JList<?> list : lists)
						if(list.isVisible())
							list.repaint();
				}
			};
			timer.addActionListener(actionListener);
			timer.setRepeats(true);
			timer.start();
		}

		public void setIcon(Object value, Icon icon) {
			if(icon != null) {
				if(loading.contains(value))
					loading.remove(value);
				icons.put(value, icon);
			} else
				icons.remove(value);
		}

		public void setLoading(Object value, boolean loading) {
			if(loading) {
				if(icons.containsKey(value))
					icons.remove(value);
				this.loading.add(value);
			} else
				this.loading.remove(value);
		}

		@Override
		public Component getListCellRendererComponent(JList<?> list,
				Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			if(!lists.contains(list))
				lists.add(list);
			JLabel label = (JLabel) super.getListCellRendererComponent(list,
					value, index, isSelected, cellHasFocus);
			Icon icon = icons.get(value);
			if(icon == null && loading.contains(value))
				icon = loadingFrames[loadingIndex];
			label.setIcon(icon);
			return label;
		}
	}

	public static class NamedImageData {
		private final Map<String, Icon> images = new HashMap<>();

		public NamedImageData() {
		}

		public NamedImageData(File file) throws IOException {
			this(new FileInputStream(file));
		}

		public NamedImageData(InputStream in) throws IOException {
			if(in == null)
				throw new NullPointerException();
			DataInputStream stream = new DataInputStream(in);
			try {
				while(stream.readBoolean()) {
					String name = stream.readUTF();
					int dataLength = stream.readInt();
					byte[] data = new byte[dataLength];
					stream.readFully(data);
					images.put(
							name,
							new ImageIcon(ImageIO
									.read(new ByteArrayInputStream(data))));
				}
			} finally {
				try {
					stream.close();
				} catch(IOException e) {}
			}
		}

		public Icon getImage(String name) {
			return images.get(name);
		}

		public void addImage(String name, Icon icon) {
			images.put(name, icon);
		}

		public void save(File file) throws IOException {
			save(new FileOutputStream(file));
		}

		public void save(OutputStream out) throws IOException {
			DataOutputStream stream = new DataOutputStream(out);
			for(String name : images.keySet()) {
				Icon icon = images.get(name);
				stream.writeBoolean(true);
				stream.writeUTF(name);
				BufferedImage image = new BufferedImage(icon.getIconWidth(),
						icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
				icon.paintIcon(null, image.createGraphics(), 0, 0);
				ByteArrayOutputStream array = new ByteArrayOutputStream();
				ImageIO.write(image, "PNG", array);
				byte[] data = array.toByteArray();
				stream.writeInt(data.length);
				stream.write(data);
			}
			stream.writeBoolean(false);
		}
	}

	private static class NamedEmergencyAction {
		private final EmergencyAction action;
		private final String name;

		public NamedEmergencyAction(EmergencyAction action, String name) {
			this.action = action;
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}
}