package org.darkstorm.runescape.script.fighter;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.SwingUtilities;

import org.darkstorm.runescape.api.input.*;
import org.darkstorm.runescape.api.tab.*;
import org.darkstorm.runescape.api.util.*;
import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.event.EventHandler;
import org.darkstorm.runescape.event.game.PaintEvent;
import org.darkstorm.runescape.script.*;
import org.darkstorm.runescape.script.fighter.Configuration.BoneAction;
import org.darkstorm.runescape.script.fighter.Configuration.EmergencyAction;
import org.darkstorm.runescape.script.paint.*;
import org.darkstorm.runescape.script.paint.Paint;
import org.darkstorm.runescape.script.paint.component.Button;
import org.darkstorm.runescape.script.paint.component.Component;
import org.darkstorm.runescape.script.paint.component.Frame;
import org.darkstorm.runescape.script.paint.component.Label;
import org.darkstorm.runescape.script.paint.component.Panel;
import org.darkstorm.runescape.script.paint.component.basic.*;
import org.darkstorm.runescape.script.paint.listener.ButtonListener;
import org.darkstorm.runescape.script.paint.theme.simple.SimpleTheme;

@ScriptManifest(name = "DarkFighter", authors = "DarkStorm",
		description = "Fight anything, anywhere, any time.", version = "1.22",
		category = ScriptCategory.COMBAT)
public class DarkFighter extends AbstractScript {
	private Configuration config;

	private Paint paint;
	private Frame frame;
	private Label timeElapsedLabel, experienceLabel, levelLabel, killsLabel;
	private Panel statusPanel;
	private Button showGuiButton;

	private long startTime;
	private int kills = 0;
	private int[] startExperience = new int[6];
	private int[] startLevels = new int[6];

	private Filter<NPC> interactionFixFilter;
	private boolean fixInteraction, fixEating;

	private static final int[] BONE_IDS = new int[] { 526, 532, 530, 534, 528,
			3125, 3183, 2859, 3123, 4812, 6729, 536, 18832 };

	public DarkFighter(ScriptManager manager) {
		super(manager);
	}

	@Override
	protected void onStart() {
		kills = 0;
		startTime = System.currentTimeMillis();
		logger.info("Started!");

		final AtomicBoolean complete = new AtomicBoolean(false);
		if(!SwingUtilities.isEventDispatchThread()) {
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						config = new SwingConfiguration(DarkFighter.this);
						if(config.awaitCompletion())
							complete.set(true);
					}
				});
			} catch(Exception e) {
				throw new RuntimeException(e);
			}
		} else {
			config = new SwingConfiguration(DarkFighter.this);
			if(config.awaitCompletion())
				complete.set(true);
		}
		if(!complete.get()) {
			stop();
			return;
		}

		for(int i = 0; i < 5; i++) {
			Skill skill = Skill.values()[i];
			startLevels[i] = skills.getActualLevel(skill);
			startExperience[i] = skills.getExperience(skill);
		}

		paint = new BasicPaint(this);
		paint.setTheme(new SimpleTheme());
		frame = new BasicFrame("DarkFighter " + getManifest().version());
		paint.addComponent(frame);
		Font font = new Font("Arial", Font.PLAIN, 14);
		try {
			font = Font
					.createFont(
							Font.TRUETYPE_FONT,
							getClass().getResource("/neuropol x free.ttf")
									.openStream()).deriveFont(14f);
		} catch(FontFormatException e1) {
			e1.printStackTrace();
		} catch(IOException e1) {
			e1.printStackTrace();
		}
		frame.setFont(font.deriveFont(Font.BOLD));
		frame.add(timeElapsedLabel = new BasicLabel("Time running: 0:00:00"));
		frame.add(new BasicLabel(" "));
		frame.add(killsLabel = new BasicLabel("Kills: 0"));
		frame.add(levelLabel = new BasicLabel("Level gain: 0"));
		frame.add(experienceLabel = new BasicLabel("Experience gain: 0"));
		frame.add(new BasicLabel(" "));
		BasicLabel status = new BasicLabel("Status:");
		frame.add(status);
		status.setFont(status.getFont().deriveFont(Font.BOLD));
		frame.add(statusPanel = new BasicPanel());
		frame.add(new BasicLabel(" "));
		showGuiButton = new BasicButton("Display configuration");
		frame.add(showGuiButton);
		showGuiButton.addButtonListener(new ButtonListener() {

			@Override
			public void onButtonPress(Button button) {
				new Thread(new Runnable() {

					@Override
					public void run() {
						pause();
						if(!SwingUtilities.isEventDispatchThread()) {
							try {
								SwingUtilities.invokeAndWait(new Runnable() {
									@Override
									public void run() {
										Configuration config = new SwingConfiguration(
												DarkFighter.this);
										if(config.awaitCompletion())
											DarkFighter.this.config = config;
									}
								});
							} catch(Exception e) {
								e.printStackTrace();
							}
						} else {
							Configuration config = new SwingConfiguration(
									DarkFighter.this);
							if(config.awaitCompletion())
								DarkFighter.this.config = config;
						}
						resume();
					}
				}).start();
			}
		});
		frame.setX(547);
		frame.setY(205);
		frame.setWidth(190);
		frame.setHeight(261);
		frame.setClosable(false);
		frame.setPinnable(false);
		frame.layoutChildren();
		Rectangle renderArea = frame.getTheme().getUIForComponent(frame)
				.getChildRenderArea(frame);
		showGuiButton.setY(renderArea.y + renderArea.height
				- showGuiButton.getHeight());
		showGuiButton.setWidth(renderArea.width);
		frame.setVisible(true);

		interactionFixFilter = new Filter<NPC>() {
			@Override
			public boolean accept(NPC value) {
				Player player = players.getSelf();
				for(int i = 0; i < 60; i++) {
					if(player.getAnimation() != -1 || player.isMoving())
						throw new RuntimeException();
					if(value.getAnimation() != -1 || value.isInCombat())
						return false;
					sleep(25);
				}
				return true;
			}
		};

		BranchTask tasks = new BranchTask(new EatTask(), new LootTask(),
				new BuryBonesTask(), new FightTask());
		register(new BranchTask(new EmergencyTask(), tasks,
				new BanPreventionTask()));
	}

	@Override
	protected void onStop() {
		logger.info("Stopped!");
		paint.dispose();
	}

	@EventHandler
	public void onPaint(PaintEvent event) {
		Graphics2D graphics = (Graphics2D) event.getGraphics();
		if(frame == null || !frame.isVisible())
			return;
		graphics.setStroke(new BasicStroke(1.0f));

		long time = (System.currentTimeMillis() - startTime) / 1000;
		long hours = time / 60 / 60, minutes = (time / 60) - (hours * 60), seconds = time
				- (minutes * 60) - (hours * 60 * 60);
		StringBuffer text = new StringBuffer();
		text.append(hours).append(":").append(minutes < 10 ? "0" : "")
				.append(minutes).append(":").append(seconds < 10 ? "0" : "")
				.append(seconds);
		timeElapsedLabel.setText("Time running: " + text.toString());

		int levels = 0, experience = 0;
		for(int i = 0; i < 5; i++) {
			Skill skill = Skill.values()[i];
			levels += skills.getActualLevel(skill) - startLevels[i];
			experience += skills.getExperience(skill) - startExperience[i];
		}
		killsLabel.setText("Kills: " + kills);
		levelLabel.setText("Level gain: " + levels);
		experienceLabel.setText("Experience gain: " + experience);

		for(Component child : statusPanel.getChildren())
			statusPanel.remove(child);
		List<Task> tasks = new ArrayList<>();
		for(Task task : getActiveTasks())
			if(task instanceof BranchTask)
				getActiveTasks((BranchTask) task, tasks);
			else
				tasks.add(task);
		for(Task task : tasks) {
			if(task instanceof FightTask)
				statusPanel.add(new BasicLabel("Attacking..."));
			else if(task instanceof EatTask)
				statusPanel.add(new BasicLabel("Eating..."));
			else if(task instanceof LootTask)
				statusPanel.add(new BasicLabel("Looting..."));
			else if(task instanceof BuryBonesTask)
				statusPanel.add(new BasicLabel("Burying bones... "));
		}
		statusPanel.resize();
		frame.layoutChildren();
		Rectangle renderArea = frame.getTheme().getUIForComponent(frame)
				.getChildRenderArea(frame);
		showGuiButton.setY(renderArea.y + renderArea.height
				- showGuiButton.getHeight());
		showGuiButton.setWidth(renderArea.width);

		paint.render(graphics);
	}

	private void getActiveTasks(BranchTask task, List<Task> activeTasks) {
		Task subtask = task.getActiveTask();
		if(subtask == null)
			return;
		if(subtask instanceof BranchTask)
			getActiveTasks((BranchTask) subtask, activeTasks);
		else
			activeTasks.add(subtask);
	}

	public NPC getInteracting() {
		if(fixInteraction)
			return null;
		String name = players.getSelf().getName();
		for(NPC npc : npcs.getAll()) {
			Animable interacting = npc.getInteractionTarget();
			if(interacting == null || !(interacting instanceof Player))
				continue;
			Player player = (Player) interacting;
			String playerName = player.getName();
			if(playerName != null && name.equals(playerName))
				return npc;
		}
		Animable interacting = players.getSelf().getInteractionTarget();
		if(interacting != null && interacting instanceof NPC)
			return (NPC) interacting;
		return null;
	}

	private class FightTask implements Task {
		private NPC lastAttacking, lastTarget;
		private Filter<NPC> failPreventionFilter;

		public FightTask() {
			failPreventionFilter = new Filter<NPC>() {
				@Override
				public boolean accept(NPC value) {
					return lastTarget == null || !value.equals(lastTarget);
				}
			};
		}

		@Override
		public boolean activate() {
			NPC interacting = getInteracting();
			if(lastAttacking != null && interacting == null) {
				kills++;
				lastAttacking = null;
			} else if(lastAttacking == null && interacting != null
					&& config.getMonsterFilter().accept(interacting))
				lastAttacking = interacting;
			Player self = players.getSelf();
			boolean valid = self.getAnimation() == -1 && !self.isMoving()
					&& !self.isInCombat();
			return interacting == null && valid;
		}

		@Override
		public void run() {
			NPC npc = getNextNpc();
			if(npc == null)
				return;
			Player self = players.getSelf();
			for(int i = 0; i < 8; i++) {
				if(self.getAnimation() != -1 || self.isMoving()
						|| self.isInCombat() || npc.isInCombat())
					return;
				sleep(25);
			}
			if(failPreventionFilter.accept(npc))
				lastTarget = null;
			if(npc.getLocation().distanceTo(self.getLocation()) > 8)
				walking.walkTo(npc.getLocation());
			else if(!npc.isOnScreen())
				camera.turnTo(npc);
			else if(!menu.perform(npc, "Attack"))
				walking.walkTo(npc.getLocation());
			lastTarget = npc;
			sleep(500, 1000);
			while(self.isMoving())
				sleep(20, 50);
		}

		private NPC getNextNpc() {
			NPC interacting = getInteracting();
			Filter<NPC> monsterFilter = filters.chain(
					config.getMonsterFilter(), failPreventionFilter);
			if(fixInteraction)
				monsterFilter = filters.chain(monsterFilter,
						interactionFixFilter);

			try {
				if(interacting == null)
					return npcs.getClosest(monsterFilter);
			} catch(Exception e) {}
			return interacting;
		}
	}

	private class EatTask implements Task {
		@Override
		public boolean activate() {
			if(fixEating)
				return false;
			int health = skills.getLevel(Skill.CONSTITUTION);
			int maxHealth = skills.getActualLevel(Skill.CONSTITUTION);
			if(!config.shouldEat(health, maxHealth))
				return false;
			Item[] food = inventory.getItems(config.getFoodFilter());
			return food.length > 0;
		}

		@Override
		public void run() {
			Item[] food = inventory.getItems(config.getFoodFilter());
			if(food.length == 0)
				return;
			if(!(game.getOpenTab() instanceof InventoryTab)) {
				game.getTab(InventoryTab.class).open();
				int failSafeInvent = 0;
				while(!(game.getOpenTab() instanceof InventoryTab)) {
					sleep(100, 150);
					failSafeInvent++;
					if(failSafeInvent > 20) {
						sleep(100);
						return;
					}
				}
			}
			int foodIndex = Math.max(0, food.length - random(1, 2));
			Item item = food[foodIndex];
			mouse.click(item);
			sleep(1000, 1500);
		}
	}

	private class LootTask implements Task {
		@Override
		public boolean activate() {
			GroundItem item = groundItems.getClosest(config.getLootFilter());
			boolean inCombat = players.getSelf().isInCombat()
					|| getInteracting() != null;
			if(fixInteraction) {
				Player self = players.getSelf();
				inCombat |= !(self.getAnimation() == -1 && !self.isMoving() && !self
						.isInCombat());
			}
			return !inventory.isFull() && item != null && !inCombat;
		}

		@Override
		public void run() {
			GroundItem item = groundItems.getClosest(config.getLootFilter());
			if(item == null || inventory.isFull() || getInteracting() != null)
				return;
			int fails = 0;
			if(calculations.isInGameArea(item.getScreenLocation())) {
				Item[] previousItems = inventory.getItems().clone();
				while(true) {
					if(fails > 5)
						return;
					GroundItem otherItem = groundItems.getClosest(config
							.getLootFilter());
					if(otherItem == null
							|| !item.getLocation().equals(
									otherItem.getLocation())
							|| inventory.isFull() || getInteracting() != null) {
						if(!(otherItem == null || !item.getLocation().equals(
								otherItem.getLocation()))
								&& inventory.isFull())
							return;
						sleep(100, 200);
						Item[] items = inventory.getItems();
						for(int i = 0; i < items.length; i++) {
							Item previous = previousItems[i];
							Item current = items[i];
							if(previous == null && current != null) {
								System.out.println("Found item: "
										+ current.getId());
								if(current.getId() != item.getId()) {
									menu.perform(current, "Drop");
									sleep(100, 200);
								}
							}
						}
						return;
					}
					try {
						if(!menu.perform(item, "Take"))
							return;
					} catch(Exception e) {
						e.printStackTrace();
						return;
					}
					sleep(800, 1000);
					int timeOutRun = 0;
					while(players.getSelf().isMoving()) {
						sleep(100, 200);
						timeOutRun++;
						if(timeOutRun > random(30, 40)) {
							sleep(100);
							break;
						}
					}
					fails++;
				}
			} else if(players.getSelf().getLocation()
					.distanceTo(item.getLocation()) < 5
					|| (players.getSelf().getLocation()
							.distanceTo(item.getLocation()) < 8 && random(0, 10) != 5)) {
				camera.setAngleXTo(item.getLocation());
				if(random(0, 3) == 1)
					camera.setAngleY(camera.getAngleY()
							+ (random(0, 200) - 100));
				sleep(100);
			} else {
				walking.walkTo(item.getLocation());
				sleep(1000, 1250);
				int timeOutRun = 0;
				while(players.getSelf().isMoving()) {
					sleep(100, 200);
					timeOutRun++;
					if(timeOutRun > random(25, 30)) {
						sleep(100);
						return;
					}
				}
			}
		}
	}

	private class BuryBonesTask implements Task {
		@Override
		public boolean activate() {
			return config.getBoneAction() == BoneAction.BURY
					&& inventory.contains(filters.item(BONE_IDS));
		}

		@Override
		public void run() {
			while(isActive() && !isPaused()) {
				Item item = inventory.getItem(filters.item(BONE_IDS));
				if(!(game.getOpenTab() instanceof InventoryTab)) {
					game.getTab(InventoryTab.class).open();
					int failSafeInvent = 0;
					while(!(game.getOpenTab() instanceof InventoryTab)) {
						sleep(100, 150);
						failSafeInvent++;
						if(failSafeInvent > 20) {
							sleep(100);
							return;
						}
					}
				}
				if(item == null)
					break;
				menu.perform(item, "Bury");
				sleep(1000, 2000);
			}
		}
	}

	private class EmergencyTask implements Task {
		@Override
		public boolean activate() {
			if(config.getCriticalHealthAction() != EmergencyAction.DO_NOTHING
					&& players.getSelf().getHealthPercentage() < 15)
				return true;
			if(config.getOutOfFoodAction() != EmergencyAction.DO_NOTHING
					&& !inventory.contains(config.getFoodFilter()))
				return true;
			return false;
		}

		@Override
		public void run() {
			if(players.getSelf().getHealthPercentage() < 15) {
				switch(config.getCriticalHealthAction()) {
				case LOGOUT:
					logout();
				case DO_NOTHING:
				}
			}
			if(!inventory.contains(config.getFoodFilter())) {
				switch(config.getOutOfFoodAction()) {
				case LOGOUT:
					logout();
				case DO_NOTHING:
				}
			}
		}

		private void logout() {
			while(getInteracting() != null) {
				int radius = 16;
				Tile tile = players.getSelf().getLocation();
				tile = new Tile(
						tile.getX()
								+ (int) (Math.cos(Math
										.toRadians(random(0, 360))) * radius),
						tile.getY()
								+ (int) (Math.sin(Math
										.toRadians(random(0, 360))) * radius));
				walking.walkTo(tile);
				sleep(250, 500);
				while(players.getSelf().isMoving())
					sleep(50, 100);
			}
			mouse.click(new RectangleMouseTarget(new Rectangle(628, 469,
					654 - 628, 497 - 469)));
			sleep(1000, 1250);
			mouse.click(new RectangleMouseTarget(new Rectangle(580, 365,
					700 - 580, 385 - 365)));
			sleep(1000, 1250);
			int failSafeLog = 0;
			while(game.isLoggedIn()) {
				sleep(100, 150);
				failSafeLog++;
				if(failSafeLog > 20) {
					sleep(100);
					return;
				}
			}
			if(!game.isLoggedIn())
				stop();
			sleep(100, 150);
		}
	}

	private class BanPreventionTask implements Task {
		private long lastAntiBan, nextAntiBan;

		public BanPreventionTask() {
			lastAntiBan = System.currentTimeMillis() - startTime;
			nextAntiBan = random(60000, 120000);
		}

		@Override
		public boolean activate() {
			return (System.currentTimeMillis() - startTime) - lastAntiBan >= nextAntiBan;
		}

		@Override
		public void run() {
			try {
				int randomMode = random(0, 4);
				if(!game.isLoggedIn())
					randomMode = 1;
				if(randomMode == 0) {
					int random = random(0, 8);
					char[] keys = new char[random > 3 ? 2 : 1];
					keys[0] = (char) (KeyEvent.VK_LEFT + (random % 4));
					if(random > 3)
						keys[1] = (char) (KeyEvent.VK_LEFT + ((random % 2) * 2));
					for(int i = 0; i < keys.length; i++)
						keyboard.pressKey(keys[i]);
					sleep(100, 1500);
					for(int i = keys.length - 1; i >= 0; i--)
						keyboard.releaseKey(keys[i]);
				} else if(randomMode == 1) {
					MouseTargetable target = null;
					int random = random(1, 5);
					if(random <= 3)
						target = npcs.getClosest(config.getMonsterFilter());
					else
						target = groundItems.getClosest(config.getLootFilter());
					if(target != null) {
						mouse.hover(target);
						mouse.await(random(1000, 2000));
						mouse.stop();
					}
				} else if(randomMode == 2) {
					for(int i = 0; i < random(1, 7); i++) {
						mouse.moveRandomly(400);
						sleep(0, 250);
					}
				} else if(randomMode == 3) {
					int random = random(1, 10);
					Tab tab;
					if(random <= 5)
						tab = game.getTab(SkillTab.class);
					else if(random == 6 || random == 7)
						tab = game.getTab(FriendTab.class);
					else if(random == 8)
						tab = game.getTab(CombatTab.class);
					else
						tab = game.getTab(EquipmentTab.class);
					tab.open();
					if(random(1, 5) != 2) {
						mouse.move(new RectangleMouseTarget(tab.getTabArea()));
						mouse.await();
					}
					sleep(3000, 6000);
					if(random(1, 5) != 2) {
						game.getTab(InventoryTab.class).open();
						sleep(250, 500);
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			lastAntiBan = System.currentTimeMillis() - startTime;
			nextAntiBan = random(7500, 50000);
		}
	}
}
