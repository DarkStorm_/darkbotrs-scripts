package org.darkstorm.runescape.script.fighter;

import org.darkstorm.runescape.api.util.Filter;
import org.darkstorm.runescape.api.wrapper.*;

interface Configuration {
	public boolean awaitCompletion();

	public Filter<NPC> getMonsterFilter();

	public Filter<GroundItem> getLootFilter();

	public Filter<Item> getFoodFilter();

	public Filter<Item> getEquipFilter();

	public boolean shouldEat(int health, int maxHealth);

	public EmergencyAction getOutOfFoodAction();

	public EmergencyAction getCriticalHealthAction();

	public BoneAction getBoneAction();

	static enum EmergencyAction {
		DO_NOTHING,
		LOGOUT
	}

	static enum BoneAction {
		NONE,
		BURY,
		BONES_TO_BANANAS,
		BONES_TO_PEACHES
	}
}
